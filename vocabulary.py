# -*- coding:utf-8 -*-
from annotations import public, private
import constants
import params
import utils

import marisa_trie
import pickle

_FMT = '<L'
_FILENAME = 'vocabulary'
_MSG_PLEASE_BUILD = 'Please build this vocabulary.'
_MSG_PLEASE_REBUILD = 'Please rebuild this vocabulary.'

@public
class Vocabulary(object):
    """
        This is the vocabulary class.
    """
    @public
    def __init__(self, include_padding_symbols=True):
        """
            Constructor.

            Parameters
            ----------
            include_padding_symbols: whether o include paddind symbols
        """
        self._words = set()
        self._trie = None
        if include_padding_symbols:
            self.add(constants.BOS)
            self.add(constants.EOS)

    @public 
    def add(self, word):
        """
            Adds a word to this vocabulary.

            Parameters
            ----------
            word: word to be added
        """
        params.check_not_null(word)
        if (self._trie is not None):
            raise Exception(_MSG_PLEASE_REBUILD)
        self._words.add(word)

    @public
    def word2id(self, word):
        """
            Gets the id of a given word.

            Parameters
            ----------
            word: input word
        """
        if (self._trie is None):
            raise Exception(_MSG_PLEASE_BUILD)
        params.check_not_null(word)
        params.check(type(word) == str)
        return self._trie.get(word)

    @public
    def id2word(self, id):
        """
            Gets a word from a given id.

            Parameters
            ----------
            id: input word id
        """
        if (self._trie is None):
            raise Exception(_MSG_PLEASE_BUILD)
        
        params.check_not_null(id)
        params.check(type(id) == int)
        params.check_not_null(self._trie)
        
        return self._trie.restore_key(id)

    @public
    def build(self):
        """
            Builds a trie (marisa_trie.Trie) out of the current vocabulary.

            Return
            ----------
            trie
        """        
        self._trie = marisa_trie.Trie(self._words) # pylint: disable=E1101

    @public
    def load(self, suffix=constants.EMPTY):
        """
            Unpickles a vocabulary from a file named 'vocabulary-$suffix$'.

            Parameters
            ----------
            suffix: file name suffix

            Return
            ----------
            vocabulary trie
        """        
        params.check_not_null(suffix)
        self._trie = utils.load_from_file(utils.concat(_FILENAME, suffix))

    @public
    def save(self, suffix=constants.EMPTY):
        """
            Pickles a vocabulary trie into a file named 'vocabulary-$suffix$'.

            Parameters
            ----------
            suffix: file name suffix
        """
        if (self._trie is None):
            raise Exception(_MSG_PLEASE_BUILD)
        params.check_not_null(suffix)
        utils.save_to_file(self._trie, utils.concat(_FILENAME, suffix))
    
    @public
    def __len__(self):
        return len(self._words) if self._trie is None else len(self._trie)