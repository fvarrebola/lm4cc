# -*- coding:utf-8 -*-
def public(f):
    return f

def private(f):
    return f

def protected(f):
    return f

def override(f):
    return f

def constructor(f):
    return f

def deprecated(f):
    return f

def fixme(f):
    return f
