# -*- coding:utf-8 -*-
from annotations import public, private, deprecated, fixme

import constants
from params import check_not_null, check, exists
import logger
import config
import utils
import db
from vocabulary import Vocabulary as Vocab

from ast import literal_eval

import argparse
import marisa_trie
import os
import pandas as pd
import pickle
import psycopg2
import re
import signal
import sys
import time

from collections import defaultdict
from github import Github
from hashlib import sha256
from javalang import tokenizer as Tokenizer
from javalang import javadoc as Javadoc
from javalang.parser import Parser
from javalang.tree import PackageDeclaration
from progressbar import ProgressBar, UnknownLength
from shutil import copy
from sklearn.model_selection import train_test_split

# -----------------------------------------------------------------------------
CMD_GIT_CLONE = "git clone --depth=1 %s %s"
CMD_GIT_LOG = "git log --pretty=format:%h"

MSG_CLONING_FAILED = "Failed to clone repository '%s'."
MSG_CLONING_REPO = "Cloning repository '%s' to path '%s'..."
MSG_CLONING_REPOSITORIES = "Cloning %s's %d repositories ..."
MSG_CLONING_SUCCEEDED = "%s: %s'"
MSG_LISTING_USER_REPOS = "Retrieving %s's repositories..."
MSG_GH_ANON_API_LIMIT_EXCEEDED = "GitHub's API anonymous user rate limit has exceeded."
MSG_GH_AUTH_API_LIMIT = "GitHub's API %s's rate limit has %d calls remaining."

MSG_OVERRIDE_IS_ON = "Override is on, so the whole process may take a while. Please wait."

MSG_NORMALIZATION_HAS_STARTED = "Normalization has started."
MSG_NORMALIZATION_ENDED = "Normalization has finished in %s s."
MSG_NORMALIZATION_STATS_ARE = "Out of %d normalizations, %d were successful, %d failed and there were %d empty files."

MSG_PARSING_HAS_STARTED = "Parsing has started."
MSG_PARSING_UPDATE = "Parsing is now at ~%.2d%%..."
MSG_PARSING_FAILED = "Failed to parse '%s'."
MSG_PARSING_STATS_ARE = "Out of %d parsings, %d were successful, %d failed, %d had empty body type declarations and %d represented empty files."

MSG_RANDOM_MSG = "? is not at ~%.2d%%..."

MSG_ALL_FILES_SHOULD_BE_PARSEABLE = "All files should be parseable. Please check \'%s\'."

MSG_BUILDING_VOCABULARY = "Building vocabulary using filter \'%s\'..."
MSG_VOCABULARY_BUILT = "A %d-word vocabulary (+2 paddings) was successfully built in %s s using filter \'%s\'."



MSG_SERIALIZING = "Serializing. Please wait."
# -----------------------------------------------------------------------------

_FILTERS = dict()
_FILTERS[constants.FILTER_NONE] = None
_FILTERS[constants.FILTER_IDENTIFIER] = Tokenizer.Identifier
_FILTERS[constants.FILTER_LITERAL] = Tokenizer.Literal
_FILTERS[constants.FILTER_SEPARATOR] = Tokenizer.Separator
_FILTERS[constants.FILTER_STRING] = Tokenizer.String

db._declare_all_classes()

@private
def signal_handler(signal, frame):
    sys.exit(constants.EXIT_CODE_CANCELLED)

signal.signal(signal.SIGINT, signal_handler)

#region github-cloning-step
@private
def _list_gh_user_repos(user, language=config.get_value(['github', 'language'])):
    """ 
        Lists all GitHub's repositories for a given user.

        Parameters
        ----------
        user: the user name
        language: the programming language

        Return
        ----------
        the list of user's repositories
    """
    logger.info(MSG_LISTING_USER_REPOS % user)
    return [repo for repo in _new_gh_client().get_user(user).get_repos() if repo.language == language]

@private 
def _new_gh_client(
        login_or_token=config.get_value(['github', 'credentials', 'login_or_token']), 
        password=config.get_value(['github', 'credentials', 'password'])):
    """ 
        Creates a new GitHub client. 
        If the the anonymous client has less than 10 requests left, we use 
        the given credentials.
        
        Parameters
        ----------
        login_or_token: where to use an explicit login or an API token
        password: the password or API token

        Return
        ----------
        a GitHub client instance
    """
    client = Github()
    if (client.get_rate_limit().rate.remaining < 10):
        logger.warn(MSG_GH_ANON_API_LIMIT_EXCEEDED)
        client = Github(login_or_token=login_or_token, password=password)
        logger.debug(MSG_GH_AUTH_API_LIMIT % (login_or_token, client.get_rate_limit().rate.remaining))

    return client

@private
def _shallow_clone_gh_repo(repository, basepath):
    """ 
        Performs a shallow clone of a repository into the given path using the 
        native 'git clone' command.

        Parameters
        ----------
        repository: the GitHub repository
        basepath: the directory

        Return
        ----------
        the last commit
    """
    check_not_null(repository, basepath)

    path = os.path.join(basepath, repository.owner.login, repository.name)
    if not os.path.exists(path):
        logger.debug(MSG_CLONING_REPO % (repository.clone_url, path))
        utils.run_native_cmd(CMD_GIT_CLONE % (repository.clone_url, path))
    
    return utils.get_cmd_output(CMD_GIT_LOG, cwd=path)[0]

@public
def clone_repositories(override_bin_files: bool = False):
    """ 
        Clones all GitHub's repositories from the set of users defined in 'config.json' file.
        
        Since repository retrieval is a time consuming operation, the list
        of repositories is pickled to file `constants.REPOSITORY_LIST`.

        Parameters
        ----------
        override_bin_files: whether to override the pickle file

        Return
        ----------
        the tuple of successful and failed clones
    """
    root = config.get_clones_dir()

    users = config.get_value(['github', 'users'])
    check(len(users) > 0)
    
    repositories = utils.load_from_file(constants.REPOSITORY_LIST)
    if repositories is None or len(repositories) < len(users) or override_bin_files:
        repositories = dict()
        for user in users:
            repositories[user] = _list_gh_user_repos(user)
        utils.save_to_file(repositories, constants.REPOSITORY_LIST)
    
    clones = dict()
    failures = dict()
    for user in users:
        clones[user] = []
        failures[user] = []
        user_repositories = repositories[user]
        logger.info(MSG_CLONING_REPOSITORIES % (user, len(user_repositories)))
        for repository in user_repositories:
            full_name = repository.full_name
            try:
                commit = _shallow_clone_gh_repo(repository, root)
                logger.info(MSG_CLONING_SUCCEEDED % (commit, full_name))
                clones[user].append((full_name, commit))
            except:
                logger.warn(MSG_CLONING_FAILED % full_name)
                failures[user].append(full_name)
    
    return (clones, failures)
#endregion

#region clone-filtering-step
@public
def filter_cloned_repositories():
    """
        Filters cloned repositories by copying .java files to the 'raw java' directory.
    """
    src_root = config.get_clones_dir()
    dst_root = config.get_filtered_dir()

    for (src_dir, _, files) in os.walk(src_root):
        # .git directories are excluded
        if (constants.GIT in src_dir): 
            continue
        
        dst_dir = src_dir.replace(src_root, dst_root, 1)
        if not os.path.exists(dst_dir): 
            os.makedirs(dst_dir)
        
        for f in files:
            src_file = os.path.join(src_dir, f)
            if (os.path.isfile(src_file)):
                (_, ext) =  os.path.splitext(src_file)
                if ext != constants.JAVA:  
                    continue
            dst_file = os.path.join(dst_dir, f)
            if os.path.exists(dst_file): 
                os.remove(dst_file)
            copy(src_file, dst_dir)
#endregion

#region normalization-step
@public
def normalize_java_files():
    """
        Normalizes every .java file found in 'raw' directory while copying the resulting file to the 'normalized' directory.
        Due to a limitation in encoding, normalization must replace null chars and all unicode surrogates.
    """
    src_root = config.get_filtered_dir()
    dst_root = config.get_normalized_dir()

    total = 0

    normalized = []
    failures = []
    empty = []

    bar = ProgressBar(max_value=UnknownLength, redirect_stdout=True)

    start = time.time()
    logger.info(MSG_NORMALIZATION_HAS_STARTED)

    for (src_dir, _, files) in os.walk(src_root):
        dst_dir = src_dir.replace(src_root, dst_root, 1)
        if not os.path.exists(dst_dir): 
            os.makedirs(dst_dir)
        for f in files:
            contents = constants.EMPTY
            src_file = os.path.join(src_dir, f)
            try:
                contents = utils.normalize(utils.read_file(src_file))
                if (len(contents) > 0):
                    path = os.path.join(dst_dir, f)
                    if (utils.write_file(path, contents)):
                        normalized.append(path)
                    else:
                        raise IOError
                else:
                    empty.append(src_file)
            except:
                failures.append(src_file)
            total += 1
        bar.update()
    bar.finish()
    
    logger.info(MSG_NORMALIZATION_ENDED % utils.elapsed(start))
    logger.info(MSG_NORMALIZATION_STATS_ARE % (total, len(normalized), len(failures), len(empty)))

    logger.info(MSG_SERIALIZING)
    utils.save_to_file(normalized, constants.NORMALIZED_FILE_LIST)
#endregion

#region parseable-files-statistics
@public
class Statistics(object):
    
    class MinMaxAvg(object):
        @public
        def __init__(self):
            self._cnt = 0
            self._min = 0
            self._max = 0
            self._total_len = 0
        
        @public
        def add(self, entry):
            check_not_null(entry)
            length = len(entry)
            if (self._min == 0 or length < self._min): 
                self._min = length
            if (self._max == 0 or length > self._max): 
                self._max = length
            self._cnt += 1
            self._total_len += length

        @property
        def cnt(self):
            return self._cnt
        
        @property
        def min(self):
            return self._min
        
        @property
        def max(self):
            return self._max

        @property
        def avg(self):
            return self._total_len / self._cnt
    
    @public
    def __init__(self):
        self._tokens = defaultdict(int)
        self._nodes = defaultdict(int)
        
        self._keywords = defaultdict(int)
        for value in Tokenizer.Keyword.VALUES:
            self._keywords[value] = 0
        
        self._identifiers = Statistics.MinMaxAvg()
        self._strings = Statistics.MinMaxAvg()

    @public
    def add_token(self, token):
        check_not_null(token)
        token_type = type(token)
        if (token_type == Tokenizer.Identifier):
            self._identifiers.add(token.value)
        elif (token_type == Tokenizer.String):
            self._strings.add(token.value)
        elif issubclass(token_type, Tokenizer.Keyword):
            self._keywords[token.value] += 1
        self._tokens[token_type.__name__] += 1
    
    @public 
    def add_node_type(self, node_type):
        check_not_null(node_type)
        self._nodes[node_type.__name__] += 1

    @property
    def tokens(self):
        return self._tokens

    @property
    def nodes(self):
        return self._nodes

    @property
    def keywords(self):
        return self._keywords
    
    @property
    def identifiers(self):
        return self._identifiers

    @property
    def strings(self):
        return self._strings


@private
def _compute_lexical_statistics(contents, statistics):
    """
        Computes lexical statistics of a compilation unit defined by its `contents`.

        Parameters
        ----------
        contents: the compilation unit contents
        statistics: the statistics object
    """
    check_not_null(contents, statistics)
    for token in Tokenizer.tokenize(contents):
        statistics.add_token(token)


@private
def _compute_semantic_statitics(root, statistics):
    """
        Computes semantic statistics of a parse tree starting at the node defined by `root`.

        Parameters
        ----------
        root: the starting node
        statistics: the statistics object
    """
    check_not_null(root, statistics)
    for (_, node) in root:
        statistics.add_node_type(type(node))


@private
def _compute_statistics(contents, unit, statistics):
    """
        Computes lexical and semantical statistics for a given compilation unit.

        Parameters
        ----------
        contents: the original file contents
        unit: the root node as a `CompilationUnit` 
        statistics: the statistics object
    """
    check_not_null(contents, unit, statistics)
    _compute_lexical_statistics(contents, statistics)
    _compute_semantic_statitics(unit, statistics)
#endregion

#region parseable-files-filtering-step
@private
def _is_valid_unit(unit):
    """
        Determines wheter the compilation unit declares at least one type with a non-empty body.

        Parameters
        ----------
        unit: compilation unit

        Return
        ----------
        whether the compilation unit is valid or not
    """
    if (hasattr(unit, 'types') and len(unit.types) > 0):
        for t in unit.types:
            if (hasattr(t, 'body') and len(t.body) > 0):
                return True
    return False

@public
def build_parseable_files_list(compute_statistics: bool = True):
    """ 
        Parses normalized java files into one single pickle file.
        
        Parameters
        ----------
        compute_statistics: whether to compute statistics
        
        Remarks
        ----------
        There is a lot of pickling going on in this method.
        First, there is the list of projects and its files, which are pickled to files
         `constants.PROJECT_LIST` and `constants.FILE_LIST`.
        Then, there is the list of parseable files which goes to file 
        `constants.PARSEABLE_FILE_LIST`, the list of failures which goes to file
        `constants.NON_PARSEABLE_FILE_LIST`, the list of empty files which goes to
        `constants.EMPTY_FILE_LIST`, and the list of files that declare types with empty bodies,
        which goes to `constants.INVALID_FILE_LIST`.
        Finally, statitistics are pickled to file `constants.STATISTICS`.
    """
    root = config.get_normalized_dir()

    logger.info("Collecting Java projects from '%s' dir..." % root)
    prj_serial = 1
    projects = []
    bar = ProgressBar(max_value=UnknownLength, redirect_stdout=True)
    for user in os.listdir(root):
        user_root = os.path.join(root, user)
        for project in os.listdir(user_root):
            projects.append((prj_serial, project, os.path.join(user_root, project)))
            prj_serial += 1
        bar.update(bar.value + 1)
    bar.finish()

    logger.info("Collecting Java files...")
    file_serial = 1
    files = []
    bar = ProgressBar(max_value=UnknownLength, redirect_stdout=True)
    for (prj_id, _, prj_path) in projects:
        for (src_dir, _, src_files) in os.walk(prj_path):
            for src_file in src_files:
                files.append((file_serial, prj_id, src_file, os.path.join(src_dir, src_file)))
                file_serial += 1
        bar.update(bar.value + 1)
    bar.finish()
    
    parseable = []
    empty = []
    invalid = []
    non_parseable = []
    
    statistics = Statistics()

    total = len(files)

    bar = ProgressBar(max_value=total, redirect_stdout=True)

    logger.info(MSG_PARSING_HAS_STARTED)

    for _file in files:
        (_, _, _, path) = _file
        try:
            contents = utils.read_file(path)
            if len(contents):
                unit = Parser(Tokenizer.tokenize(contents)).parse()
                if (_is_valid_unit(unit)):
                    parseable.append(_file + (sha256(contents.encode()).hexdigest(), ))
                    if (compute_statistics):
                        _compute_statistics(contents, unit, statistics)
                else:
                    invalid.append(_file)
            else:
                empty.append(_file)
        except:
            non_parseable.append(_file)
        bar.update(bar.value + 1)
    bar.finish()
    
    logger.info(MSG_PARSING_STATS_ARE % (total, len(parseable), len(non_parseable), len(invalid), len(empty)))

    logger.info(MSG_SERIALIZING)
    utils.save_to_file(projects, constants.PROJECT_LIST)
    utils.save_to_file(files, constants.FILE_LIST)
    utils.save_to_file(parseable, constants.PARSEABLE_FILE_LIST)
    utils.save_to_file(empty, constants.EMPTY_FILE_LIST)
    utils.save_to_file(invalid, constants.INVALID_FILE_LIST)
    utils.save_to_file(non_parseable, constants.NON_PARSEABLE_FILE_LIST)
    utils.save_to_file(statistics, constants.STATISTICS)
#endregion

@private
def _get_first_parent_match(child: Tokenizer.JavaToken, candidates: list):
    for _candidate in candidates:
        _type = _FILTERS[_candidate]
        if _type is not None and issubclass(child, _type):
            return _type
    return None

@private
def _tokenize_using_filters(contents: str, inclusion_filters: list = [], substitution_filters: list = []) -> list:
    """
        Tokenizes a string using the `javalang.tokenizer` while applying token type inclusion and substitution filters.

        Parameters
        ----------
        contents: input string to be tokenized
        inclusion_filters: list of strings representing the token types to consider while tokenizing. 
        If unset all token types are interpreted.
        substitution_filters: list of strings representing the token types to substitute. 
        If set, token values are replaced with the `<$TOKEN_TYPE>` replacement string.

        Return
        ----------
        the tokenized sentence as a list of 2-tuples comprising the type name and the token value
    """
    check_not_null(contents)

    use_inclusion_filters = len(inclusion_filters) > 0
    use_substitution_filters = len(substitution_filters) > 0

    sentence = []

    fn_replace = utils.replace_null_chars
    fn_append = sentence.append
    for token in Tokenizer.tokenize(contents):
        
        token_class = token.__class__
        type_name = token_class.__name__
        token_value = constants.EMPTY
        
        if (use_inclusion_filters and _get_first_parent_match(token_class, inclusion_filters) is None):
            continue

        if (use_substitution_filters):
            parent = _get_first_parent_match(token_class, substitution_filters)
            if (parent is not None):
                token_value =  '<%s>' % parent.__name__.upper()
        
        if (token_value == constants.EMPTY):
            token_value =  fn_replace(token.value)
        
        fn_append((type_name, token_value))
    
    return sentence

@public 
def build_vocabulary(args, **kwargs):
    """
        Builds a vocabulary trie from all parseable java files for each available tokenization filter.
        
        Remarks
        ----------
        The list of parseable java files is unpickled from file `constants.PARSEABLE_FILE_LIST`.

        Vocabularies are `marisa_trie.RecordTrie` tries and they are pickled to files that later will be used to build trainning and evaluation sentences.
    """
    (filter_name, show_progress_bar) = (args.filter_name, args.show_progress_bar)

    files = utils.load_from_file(constants.PARSEABLE_FILE_LIST)
    total = len(files)
    check(total > 0)

    logger.info(MSG_BUILDING_VOCABULARY % filter_name)
    
    vocab = Vocab()
    if show_progress_bar:
        bar = ProgressBar(max_value=total, redirect_stdout=True)
        fn_update_bar = bar.update
    
    fn_read_file = utils.read_file
    fn_add = vocab.add
    tokenization_filters = [] if filter_name == constants.FILTER_NONE else filter_name.split("+")
    
    for (_, _, _, path, _) in files:
        contents = fn_read_file(path)
        if len(contents):
            try:
                for (_, word) in _tokenize_using_filters(contents, substitution_filters=tokenization_filters):
                    fn_add(word)
            except:
                raise Exception(MSG_ALL_FILES_SHOULD_BE_PARSEABLE % path)
        if show_progress_bar:
            fn_update_bar(bar.value + 1)
    if show_progress_bar:
        bar.finish()

    vocab.build()

    logger.info(MSG_SERIALIZING)
    vocab.save(filter_name)

@public
def build_sentences(args, **kwargs):
    """
        Builds trie-mapped sentences from parseable java files for each available tokenization filter.

        Remarks
        ----------
        The list of parseable java files is unplckled from file `constants.PARSEABLE_FILES`.

        All sentences are then exported to .CSV files that could be copied to a PostgresSQL Docker container for faster tranning and evaluation.
    """
    (filter_name, show_progress_bar) = (args.filter_name, args.show_progress_bar)
         
    files = utils.load_from_file(constants.PARSEABLE_FILE_LIST)
    total = len(files)
    check(total > 0)

    logger.info("Building sentences using filter '%s'..." % filter_name)
    filter_id = db.get_filter_by_name(filter_name).id

    qty = db.qty(db.Sentence) + 1 # pylint: disable=E1101
    logger.info("Sentences indexes will start at %d..." % qty)

    logger.info("Loading vocabulary...")
    vocab = Vocab()
    vocab.load(filter_name)

    if show_progress_bar:
        bar = ProgressBar(max_value=total, redirect_stdout=True)
        fn_update_bar = bar.update

    df = pd.DataFrame()

    fn_read_file = utils.read_file
    fn_w2id = vocab.word2id
    tokenization_filters = [] if filter_name == constants.FILTER_NONE else filter_name.split("+")
    for (idx, (file_id, _, _, path, _)) in enumerate(files):
        try:
            contents = fn_read_file(path)
            if len(contents):
                sentence = _tokenize_using_filters(contents, substitution_filters=tokenization_filters)
                mapped_sentence = list(map(lambda x: fn_w2id(x[1]), sentence))
                df = df.append(pd.DataFrame([qty + idx, filter_id, file_id, str(mapped_sentence)]).T)
            else:
                raise Exception
        except:
            raise Exception(MSG_ALL_FILES_SHOULD_BE_PARSEABLE % path)
        if show_progress_bar:
            fn_update_bar(bar.value + 1)
    
    if show_progress_bar:
        bar.finish()

    path = os.path.join(config.get_csv_dir(), ('sentences-%s' % filter_name).lower())
    logger.info("Saving sentences to file '%s'..." % path)
    df.to_csv(path, sep='\t', mode='a', header=False, index=False)

    logger.info("All %d sentences were successfully built." % total)

@public
def build_train_test_split(args, **kwargs):
    
    (filter_name, prune_min) = (args.filter_name, args.prune_min)

    filter_id = db.get_filter_by_name(filter_name).id

    # global split
    logger.info("Spliting sentences built with filter '%s'..." % filter_name)
    sentences = db.get_sentence_ids(filter_id)
    total = len(sentences)
    if (total):
        (train, test) = train_test_split(sentences, random_state=constants.RANDOM_STATE)
        db.add_train_test_split(filter_id=filter_id, train_size=len(train), train_data=str(train), test_size=len(test), test_data=str(test))

    # per project split
    for prj in db.get_project_list():
        sentences = db.get_sentence_ids(filter_id, prj.id)
        total = len(sentences)
        if (total >= prune_min):
            (train, test) = train_test_split(sentences, random_state=constants.RANDOM_STATE)
            db.add_project_train_test_split(user_id=prj.user_id, project_id=prj.id, filter_id=filter_id, train_size=len(train), train_data=str(train), test_size=len(test), test_data=str(test))


@deprecated
def _build_table_name(csv_file):
    table = csv_file[:csv_file.index('.')]
    if '-' in table:
        table = table[:table.index('-')]
    return table[:len(table) - 1]

_MSG_VOCAB_ADDITION_STARTED = "Vocabulary '%s' insertion has started..."
_MSG_VOCAB_ADDITION_ENDED = "Vocabulary inserting has ended in %s s."
_MSG_PSQL_COPY_STARTED = "PSQL COPY from '%s' to '%s' has started..."
_MSG_PSQL_COPY_ENDED = "PSQL COPY has ended in %s s."


@private 
@fixme
def _insert_vocabulary_into_psql():
    for filter_name in constants.FILTER_NAMES:
        
        start = time.time()
        name = utils.concat('vocabulary', filter_name)

        logger.info(_MSG_VOCAB_ADDITION_STARTED % name)
        
        tx = db.new_tx()
        vocab = Vocab()
        vocab.load(filter_name)
        db.add_trie(name, vocab._trie, tx)
        tx.commit()
        
        logger.info(_MSG_VOCAB_ADDITION_ENDED % utils.elapsed(start))


@deprecated
def _copy_sentences_to_psql():
    """
        Copies .CSV files to a PostgresSQL instance using COPY command.
    """
    conn = psycopg2.connect(config.get_sql_url())

    root = ''
    for csv_file in os.listdir(root):

        if constants.CSV not in csv_file:
            continue

        table = _build_table_name(csv_file)
        path = os.path.join(root, csv_file)

        logger.info("Copying file '%s' into table '%s'..." % (csv_file, table))

        with conn:
            with conn.cursor() as cursor:
                with open(path, encoding=constants.ENCODING) as fp:
                    cursor.copy_from(fp, table)


@public
def init_db():
    _insert_vocabulary_into_psql()
    _copy_sentences_to_psql()

#region topic_modeling
_REGEXP_FOR_CAMEL_CASE_SPLITTING = re.compile('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)')
_REGEXP_FOR_UNDERLINE_SPLITTING = re.compile('[_]+')
_REGEXP_FOR_JAVADOC_TAGS = re.compile('@(author|code|docRoot|deprecated|exception|inheritDoc|link|linkplain|literal|param|return|see|serial|serialData|serialField|since|throws|value|version)')
_REGEXP_FOR_HTML_TAGS = re.compile('<[^>]*>')
_REGEXP_FOR_SPACES = re.compile(r'\s{2,}')
_REGEXP_FOR_SYMBOLS = re.compile(r'[\t\"\!\@\#\$\%\&\¨\*\(\)\-\_\+\=\[\{\]\}\`\´\^\~\<\>\;\:\/\?\\\|]')

@private
def _split_into_terms(identifier: str, lower_threshold: int = constants.LOWER_THRESHOLD, upper_threshold: int = constants.UPPER_THRESHOLD) -> list:
    """
        Splits an identifier into a list of terms using camel case and underline splitting.
        
        Parameters
        ----------
        identifier: original identifier
        lower_threshold: exclusive lower threshold of a term after splitting
        upper_threshold: exclusive upper threshold of a term after splitting

        Return
        ----------
        list of lower case terms
    """
    check_not_null(identifier)
    
    terms = []
    for term in [part for part in identifier.split('.') if part]:
        matches = _REGEXP_FOR_CAMEL_CASE_SPLITTING.finditer(term)
        for hit in [m.group(0) for m in matches]:
            for entry in _REGEXP_FOR_UNDERLINE_SPLITTING.split(hit):
                length = len(entry)
                if ((lower_threshold > constants.LOWER_THRESHOLD and length < lower_threshold) or
                    (upper_threshold > constants.UPPER_THRESHOLD and length > upper_threshold)):
                    continue
                terms.append(entry) 
    return list(map(lambda x: x.lower(), terms))

@private
def _parse_identifiers(sentence: tuple) -> list:
    """
        Prepares a sentence for topic modeling by splitting every term accoring to `_split_into_terms` rules and after checking whether the token is acctually an identifier.

        Parameters
        ----------
        sentence: sentence to prepare

        Return
        ----------
        list of terms
    """
    terms = []
    for (token_type, token_value) in sentence:
        if (token_type == Tokenizer.Identifier):
            raise ValueError()
        for term in _split_into_terms(token_value):
            terms.append(term)
    return terms

@private
def _normalize(contents: str, replace_javadoc_tags: bool = True, replace_html_tags: bool = True, remove_symbols: bool = True, remove_extra_spaces: bool = True) -> list:
    """
       Normalizes a string by replacing a list of symbols with an empty char.
    """
    if replace_javadoc_tags:
        contents = _REGEXP_FOR_JAVADOC_TAGS.sub(constants.EMPTY, contents)

    if replace_html_tags:
        contents = _REGEXP_FOR_HTML_TAGS.sub(constants.EMPTY, contents)

    if remove_symbols:
        contents = _REGEXP_FOR_SYMBOLS.sub(constants.EMPTY, contents)
    
    if remove_extra_spaces:
        contents = _REGEXP_FOR_SPACES.sub(constants.SPACE, contents)
    
    normalized = []
    for raw_term in contents.split():
        _ = [normalized.append(term) for term in _split_into_terms(raw_term)]
    
    return normalized

@private
def _append_to_comments(data, comments: list):
    """
        Appends all available comments in `data` to a list.
    """
    if not data:
        return
    if type(data) == str:
        _ = [comments.append(term) for term in _normalize(data)]
    elif type(data) == list:
        for el in data:
            _append_to_comments(el, comments)
    elif type(data) == dict:
        for (key, value) in data.items():
            _ = [comments.append(term) for term in _normalize(key)]
            _ = [comments.append(term) for term in _normalize(value)]
    
@private
def _parse_comments(contents: str) -> list:
    """
        Parses javadoc blocks found in a CompilationUnit.

        Return
        ----------
        list of comments
    """
    comments = []
    unit = Parser(Tokenizer.tokenize(contents)).parse()
    try:
        for (_, node) in unit:
            if type(node) == PackageDeclaration:
                continue
            if hasattr(node, 'documentation') and node.documentation:
                block = Javadoc.parse(node.documentation)
                for attr in ['description', 'params', 'return_doc', 'throws']:
                    _append_to_comments(getattr(block, attr, None), comments)
    except RecursionError:
        logger.warn("Ignoring compilation unit comments due to a recursion error.")
        comments = []
    
    return comments

@public
def build_sentences_for_topic_modeling(args, **kwargs):
    """
        Builds sentences for topic modeling.
    """
    (include_javadocs, include_identifiers, prune_min, prune_max, show_progress_bar) = \
         (args.include_javadocs, args.include_identifiers, args.prune_min, args.prune_max, args.show_progress_bar)

    check(include_javadocs or include_identifiers)

    parseable_files = utils.load_from_file(constants.PARSEABLE_FILE_LIST)
    total = len(parseable_files)
    check(total > 0)
    
    qty = db.qty(db.SentenceForTopicModeling) + 1 # pylint: disable=E1101
    logger.info("Sentences indexes will start at %d..." % qty)

    logger.info("Building sentences over %d files..." % total)
    if show_progress_bar:
        bar = ProgressBar(max_value=total, redirect_stdout=True)
        fn_update = bar.update
    
    df = pd.DataFrame()

    fn_read_file = utils.read_file
    for (idx, (file_id, prj_id, _, path, _)) in enumerate(parseable_files):
        try:
            contents = fn_read_file(path)
            if len(contents):
                sentence = []
                if include_javadocs:
                    sentence = _parse_comments(contents)
                if include_identifiers:
                    sentence += _parse_identifiers(_tokenize_using_filters(contents, inclusion_filters=[constants.FILTER_IDENTIFIER]))
                df = df.append(pd.DataFrame([idx + 1, file_id, prj_id, ' '.join(sentence)]).T)
            else:
                raise Exception
        except:
            raise Exception(MSG_ALL_FILES_SHOULD_BE_PARSEABLE % path)
        
        if show_progress_bar:
            fn_update(bar.value + 1)
    
    if show_progress_bar:
        bar.finish()

    path = os.path.join(config.get_csv_dir(), 'topic-modeling-%d-sentences' % total)
    logger.info("Saving sentences to file '%s'..." % path)
    df.to_csv(path, sep='\t', mode='a', header=False, index=False)

#endregion

#region data_loading
@public
def load_sentences(filter_name: str, **kwargs) -> tuple:
    """
        Loads all training and testing sentences plus the vocabulary for a given token filter.

        Parameters
        ----------
        filter_name: token filter name

        Return
        ----------
        tuple containing the tranning and testing sentences plus the complete vocabulary
    """
    check_not_null(filter_name)

    negate_filters = kwargs.get('negate_filters', False)
    ignore_project_level = kwargs.get('ignore_project_level', False)

    vocab = Vocab()
    vocab.load(filter_name)
    if not len(vocab):
        vocab = db.get_trie(filter_name)

    filter_id = db.get_filter_by_name(filter_name).id

    if ignore_project_level:
        entry = db.get_train_test_split(filter_id=filter_id)
        return (entry.train_data, entry.test_data, vocab)

    user_id = 0
    user_name = kwargs.get('user', None)
    if user_name:
        user_id = db.get_user(name=user_name).id

    project_id = 0
    project_name = kwargs.get('project', None)
    if project_name:
        project_id = db.get_project(name=project_name).id
    
    train_data = []
    test_data = []
    
    logger.info("Loading sentences using params %s..." % kwargs)
    for entry in db.get_train_test_split_per_project(filter_id=filter_id):
        skip = False
        if negate_filters:
            skip = (project_id == entry.project_id or user_id == entry.user_id)
        else:
            if project_id and project_id != entry.project_id:
                skip = True
            if user_id and user_id != entry.user_id:
                skip = True
        if skip:
            continue
        train_data += literal_eval(entry.train_data)
        test_data += literal_eval(entry.test_data)

    logger.info("Loaded %d/%d training/testing sentences." % (len(train_data), len(test_data)))

    return (train_data, test_data, vocab)
#endregion

#region main
@public
def main(args: argparse.Namespace):
    """
    """
    check_not_null(args)

    kwargs = {}

    if (args.clone):
        clone_repositories()
    elif (args.filter):
        filter_cloned_repositories()
    elif (args.normalize):
        normalize_java_files()
    elif (args.parse):
        build_parseable_files_list()
    elif (args.lm_vocab):
        build_vocabulary(args, **kwargs)
    elif (args.lm_sentences):
        build_sentences(args, **kwargs)
    elif (args.topic_model_sents):
        build_sentences_for_topic_modeling(args, **kwargs)
    elif (args.split):
        build_train_test_split(args, **kwargs)
    elif (args.psql):
        init_db()

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='This script helps you build a working corpora.\n')

    group = parser.add_mutually_exclusive_group(required=True)
    
    # data acquisition operations
    group.add_argument('-clone', action='store_true', help=utils.get_doc(clone_repositories))
    group.add_argument('-filter', action='store_true', help=utils.get_doc(filter_cloned_repositories))
    group.add_argument('-normalize', action='store_true', help=utils.get_doc(normalize_java_files))
    group.add_argument('-parse', action='store_true', help=utils.get_doc(build_parseable_files_list))

    # language model operations
    group.add_argument('-lm-vocab', action='store_true', help=utils.get_doc(build_vocabulary))
    group.add_argument('-lm-sentences', action='store_true', help=utils.get_doc(build_sentences))

    # topic modeling operations 
    group.add_argument('-topic-model-sents', action='store_true', help=utils.get_doc(build_sentences_for_topic_modeling))
    
    group.add_argument('-split', action='store_true', help=utils.get_doc(build_train_test_split))
    group.add_argument('-psql', action='store_true', help=utils.get_doc(init_db))

    # optional arguments
    parser.add_argument('--filter-name', choices=constants.FILTER_NAMES)
    parser.add_argument('--include-javadocs', action='store_true', default=True)
    parser.add_argument('--include-identifiers', action='store_true', default=False)
    parser.add_argument('--prune-min', type=int, default=1)
    parser.add_argument('--prune-max', type=int, default=None)
    parser.add_argument('--show-progress-bar', action='store_true', default=True)

    main(parser.parse_args())

    sys.exit(constants.EXIT_CODE_SUCCESS)
#endregion