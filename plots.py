from annotations import public
from config import get_csv_dir
import logger
from params import check_not_null

import argparse
import fnmatch
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import os
import pandas as pd

CE_DEFAULT_PARAMS = {
    'title': 'Entropia cruzada por ngrama versus\nquantidade de sentenças utilizadas durante o treinamento',
    'title-multi-files': 'Entropia cruzada por algoritmo para ngramas de order %d versus\nquantidade de sentenças utilizadas durante o treinamento',
    'xlabel': 'Quantidade de sentenças utilizadas durante o treinamento',
    'ylabel': 'Entropia cruzada (bits)',
    'ycol': 'ce',
    'yformatter': '%.2f'
}

PPL_DEFAULT_PARAMS = {
    'title': 'Perplexidade por ngrama versus\nquantidade de sentenças utilizadas durante o treinamento',
    'title-multi-files': 'Perplexidade por algoritmo para ngramas de order %d versus\nquantidade de sentenças utilizadas durante o treinamento',
    'xlabel': 'Quantidade de sentenças utilizadas durante o treinamento',
    'ylabel': 'Perplexidade',
    'ycol': 'ppl',
    'yformatter': '%d'
}   

MRR_DEFAULT_PARAMS = {
    'title': 'Mean Reciprocal Rank (MRR) por ngrama versus\nquantidade de sentenças utilizadas durante o treinamento',
    'title-multi-files': 'Mean Reciprocal Rank (MRR) por algoritmo para ngramas de order %d versus\nquantidade de sentenças utilizadas durante o treinamento',
    'xlabel': 'Quantidade de sentenças utilizadas durante o treinamento',
    'ylabel': 'Mean Reciprocal Rank (MRR)',
    'ycol': 'mrr',
    'yformatter': '%.2f',
    'ymin': 0,
    'ymax': 1.0
}

PARAMS = { 'ce': CE_DEFAULT_PARAMS, 'ppl': PPL_DEFAULT_PARAMS, 'mrr': MRR_DEFAULT_PARAMS }

ALGS = {
    'addonesmoothing': 'AddOne (Laplace Smoothing)', 
    'heuristicbackoff': 'BackOff (com heurística)', 
    'naivebackoff': 'BackOff (ingênuo)', 
    'interpolatedwithconditionallambdas': 'Interpolated (\u03bb condicionais)', 
    'kneserney': 'KneserNey (padrão)'}

TITLE_FONT_DICT = {'size': 'small', 'weight': 'bold'}
AXIS_FONT_DICT = {'size': 'smaller'}

@public
def build_lm_plot_from_multiple_files(fileset, args):
    
    check_not_null(fileset, args)
    
    plot_params = PARAMS[args.plot_params]

    df_set = {}
    df_filtered = {}
    df_final = pd.DataFrame()

    for filename in fileset:
        
        path = os.path.join(get_csv_dir(), filename)
        logger.info("Loading data from file '%s'..." % path)

        alg = None
        for (k, v) in ALGS.items():
            if k in filename:
                alg = v
        if not alg:
            raise Exception
        
        df_set[alg] = pd.read_csv(path, sep='\t', index_col=False)
        df_filtered[alg] = df_set[alg].query('order==%d' % args.order)
        df_filtered[alg]['alg'] = alg

    for (k, v) in df_filtered.items():
        df_final = df_final.append(v)

    (_, axis) = plt.subplots(1,1)
    axis.yaxis.set_major_formatter(mtick.FormatStrFormatter(plot_params['yformatter']))
    
    title = (plot_params['title-multi-files'] % args.order).upper()
    if args.subtitle:
        title = '%s\n%s' % (title, args.subtitle)
    
    plt.title(title, fontdict=TITLE_FONT_DICT)

    _ = df_final.groupby('alg').plot(x='train', y=plot_params['ycol'], ax=axis)
    labels = [v[0] for v in df_final.groupby('alg')['alg']]

    if args.log:
        plt.xscale('log')

    if 'ymin' in plot_params:
        plt.ylim(ymin=plot_params['ymin'])
    
    if 'ymax' in plot_params:
        plt.ylim(ymax=plot_params['ymax'])

    plt.grid(True, which='major', linewidth=0.25)
    plt.grid(b=True, which='minor', linewidth=0.1)
    plt.minorticks_on()
    plt.xlabel(plot_params['xlabel'].upper(), fontdict=AXIS_FONT_DICT)
    plt.ylabel(plot_params['ylabel'].upper(), fontdict=AXIS_FONT_DICT)
    plt.legend(labels, loc='upper right', fontsize='xx-small')

    output = '%s-mutiple-files.png' % plot_params['ycol']

    logger.info("Saving plot to file '%s'..." % output)
    plt.savefig(os.path.join(get_csv_dir(), output))


@public
def build_lm_plot_from_single_file(filename, args):

    check_not_null(filename, args)

    plot_params = PARAMS[args.plot_params]

    path = os.path.join(get_csv_dir(), filename)
    
    logger.info("Loading data from file '%s'..." % path)
    df = pd.read_csv(path, sep='\t', index_col=False)

    (_, axis) = plt.subplots(1,1)
    axis.yaxis.set_major_formatter(mtick.FormatStrFormatter(plot_params['yformatter']))

    title = plot_params['title'].upper()
    if args.subtitle:
        title = '%s\n%s' % (title, args.subtitle)
    plt.title(title, fontdict=TITLE_FONT_DICT)

    _ = df.groupby('order').plot(x='train', y=plot_params['ycol'], ax=axis)
    labels = ['n=%d' % v[0] for v in df.groupby('order')['order']]

    if args.log:
        plt.xscale('log')

    if 'ymin' in plot_params:
        plt.ylim(ymin=plot_params['ymin'])
    
    if 'ymax' in plot_params:
        plt.ylim(ymax=plot_params['ymax'])

    plt.grid(True, which='major', linewidth=0.25)
    plt.grid(b=True, which='minor', linewidth=0.1)
    plt.minorticks_on()
    plt.xlabel(plot_params['xlabel'].upper(), fontdict=AXIS_FONT_DICT)
    plt.ylabel(plot_params['ylabel'].upper(), fontdict=AXIS_FONT_DICT)
    plt.legend(labels, loc='upper right', fontsize='x-small', ncol=2)

    output = '%s-%s.png' % (plot_params['ycol'], filename)

    logger.info("Saving plot to file '%s'..." % output)
    plt.savefig(os.path.join(get_csv_dir(), output))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(\
        description='This script helps you build plots.',\
        formatter_class=argparse.RawTextHelpFormatter)

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-from-single-file', action='store_true')
    group.add_argument('-from-multiple-files', action='store_true')
    parser.add_argument('--pattern', type=str, default='*.csv')
    parser.add_argument('--plot-params', choices=PARAMS.keys())
    parser.add_argument('--subtitle', type=str, default=None)
    parser.add_argument('--order', type=int, default=None)
    parser.add_argument('--log', action='store_true', default=False)

    args = parser.parse_args()

    if args.from_single_file:
        files = fnmatch.filter(os.listdir(get_csv_dir()), args.pattern)
        for filename in files:
            build_lm_plot_from_single_file(filename, args)
    elif args.from_multiple_files:
        build_lm_plot_from_multiple_files(fnmatch.filter(os.listdir(get_csv_dir()), args.pattern), args)
    else:
        parser.print_help()
