# -*- coding:utf-8 -*-
#!/usr/bin/env python3
from annotations import public, private
import config
import constants
import logger
import params

from os.path import splitext
from pathlib import Path
from subprocess import run, PIPE

import codecs
import javalang
import json
import os
import pickle
import re
import tempfile
import time

MSG_NATIVE_CMD_TRACE = "Native command '%s' exit code is %d."

@private
def _is_windows():
    """ 
        Checks whether the os name is 'nt'.

        Return
        ----------
        true if the os name is 'nt'; false otherwise
    """
    return (os.name == 'nt') 


@private
def _exists(cmd):
    """ 
        Checks whether a given native command exists using 'which' tool.

        Parameters
        ----------
        cmd: the native command

        Return
        ----------
        true if the command exists; false otherwise
    """
    args = ('which %s%s' % (cmd, ('.exe' if _is_windows() else ''))).split()
    return (run(args, stdout=PIPE, stderr=None, check=True).returncode == 0)


@public
def abbrev_filename(filename, max=50):
    """
        Abbreviates a file name to the maximum number of chars given.

        Parameters
        ----------
        filename: the file name to abbreviate
        max: the maximum number of chars

        Return
        ----------
        the abbreviated file name
    """
    _abbrev = filename
    if len(filename) > max:
        _abbrev = ('%s...%s' % (filename[:25], os.path.basename(filename)))

    return _abbrev


@public
def concat(prefix, suffix):
    """
        Parameters
        ----------
        Return
        ----------
    """
    params.check_not_empty(prefix)
    return ('%s-%s' % (prefix.lower(), suffix.lower())) if (len(prefix) > 0 and len(suffix) > 0) else prefix.lower()


@public
def load_from_file(filename):
    """ 
        Unpickles a file to an object.

        Parameters
        ----------
        filename: the input file name

        Return
        ----------
        the object
    """
    params.check_not_null(filename)
    
    obj = None
    path = _get_pickle_file(filename)
    if os.path.exists(path) and os.path.isfile(path):
        with open(path, 'rb') as f:
            obj = pickle.load(f)
    else:
        raise FileNotFoundError(path)
    
    return obj


@public
def json_from_file(filename):
    """ 
        Loads a JSON object from a file.

        Parameters
        ----------
        filename: the input file name

        Return
        ----------
        the json object
    """
    params.check_not_null(filename)
    data = None
    try:
        with open(filename, 'r') as f:
            data = json.load(f)
    except:
        pass
    
    return data


@public
def json_to_file(data, filename):
    """ 
        Dumps a JSON object to a file.

        Parameters
        ----------
        data: the data to be serialized
        filename: the output file name
    """
    params.check_not_null(data, filename)
    with open(filename, 'w') as f:
        json.dump(data, f, indent=2, sort_keys=True)


@public
def load_class(name):
    """ 
        Loads a class by the given name.

        Parameters
        ----------
        name: the fully qualified class name

        Return
        ----------
        an instance of the given class
    """
    params.check_not_null(name)
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


@public
def get_cmd_output(cmd, cwd):
    """
        Reads the output of a native command.

        Parameters
        ----------
        cmd: the native command
        cwd: the base directory

        Return
        ----------
        the command output as a list
    """
    return run_native_cmd(cmd, cwd=cwd).stdout.decode(constants.ENCODING).strip().splitlines()


@public
def elapsed(start):
    """
        Parameters
        ----------
        Return
        ----------
    """
    params.check_not_null(start)
    return '%.2f' % (time.time() - start)


@public
def get_tempdir(dirname):
    """
        Parameters
        ----------
        Return
        ----------
    """
    params.check_not_null(dirname)
    return os.path.join(tempfile.gettempdir(), dirname)


@public
def build_file_path(root, filename, ext):
    """
        Builds a path starting at `root` and including the `filename` and `ext` given.

        Parameters
        ----------
        root: the root directory
        filename: the file name
        ext: the file extension

        Return
        ----------
        the final path
    """
    params.check_not_null(root, filename, ext)
    
    _ext = '.%s' % ext
    if _ext not in filename:
        filename = (('%s.%s') % (filename, ext))
    
    return os.path.join(root, filename)


@public
def build_temp_file_path(dirname, filename, ext):
    return 


@public
def milestones(total=0, qty=4):
    """
        Computes milestones given the total number of steps and the 
        expected number of milestones.

        Parameters
        ----------
        total: the total number of steps
        qty: the expected number of milestones

        Return
        ----------
        list of milestones
    """
    step = total / float(qty)
    milestones = []
    for x in range(0, qty):
        milestones.append(int((x + 1) * step))
    milestones[qty - 1] = total
    return milestones


# pre-compiled regular expressions
REGEXP_NON_ASCII = re.compile(r'[^\x00-\x7f]')
REGEXP_UNICODE_ESCAPES = re.compile(r'[\\]{1,2}[u][A-Fa-f0-9]{4}')
REGEXP_UNICODE_SURROGATES = re.compile(r'[\\]{1,2}[u][dD][A-Fa-f8-9][A-Fa-f0-9]{2}')


@public
def normalize(contents, replace_nulls=True, remove_non_ascii=True, remove_unicode_escapes=True, remove_surrogates=True):
    """
        Normalizes an input string by removing null chars, non-ascii chars, unicode escapes and surrogates.

        Parameters
        ----------
        contents: the input string
        replace_nulls: whether to remove null chars
        remove_non_ascii: whether to remove non-ascii chars
        remove_unicode_escapes: whether to remove unicode escape chars
        remove_surrogates: whether to remove unicde surrogates

        Return
        ----------
        a normalized version of the input string
    """
    if replace_nulls:
        contents = replace_null_chars(contents)
    
    if remove_non_ascii:
        contents = REGEXP_NON_ASCII.sub(constants.EMPTY, contents)

    if remove_unicode_escapes:
        contents = REGEXP_UNICODE_ESCAPES.sub(constants.EMPTY, contents)
    
    if remove_surrogates:
        contents = REGEXP_UNICODE_SURROGATES.sub(constants.EMPTY, contents)

    return contents


@public
def read_file(path, mode='r', encoding=constants.ENCODING):
    """
        Reads a file.

        Parameters
        ----------
        path: the file path
        mode: the file opening mode (defaults to 'r')
        encoding: the encoding (defaults to `constants.ENCODING`)

        Return
        ----------
        the file data
    """
    params.check_not_null(path)
    data = None
    with open(path, mode, encoding=encoding) as f:
        data = f.read()
    return data


@public
def replace_null_chars(contents):
    """
        Replaces all null chars characters of a given string.

        Parameters
        ----------
        contents: the input string
        
        Return
        ----------
        a null-free version of the input string
    """
    return contents.replace(constants.NULL_CHAR, constants.SPACE) \
        if constants.NULL_CHAR in contents else contents


@public
def run_native_cmd(cmd, input=None, stdout=PIPE, stderr=PIPE, cwd=str(Path.cwd()), check=True):
    """ 
        Runs the native command `cmd`.

        Parameters
        ----------
        cmd: the native command
        input: the command input
        stdout: the stdout stream
        stdin: the stdin stream
        cwd: the base directory
        check: whether to check the command return code

        Return
        ----------
        the process
    """
    args = cmd.split()
    if len(args) == 0:
        return None
    if not _exists(args[0]):
        return None
    
    proc = run(args, input=input, stdout=stdout, stderr=stderr, cwd=cwd, check=check)
    if config.get_value(['trace-native-cmd']):
        logger.info(MSG_NATIVE_CMD_TRACE % (' '.join(args), proc.returncode))

    return proc

@private
def _get_pickle_file(filename):
    return build_file_path(config.get_bin_dir(), filename, constants.BIN) 

@public
def save_to_file(obj, filename):
    """ 
        Pickles an object to a file.

        Parameters
        ----------
        obj: the object
        filename: the output file name
    """
    params.check_not_null(filename)
    path = _get_pickle_file(filename)
    with open(path, 'wb') as f:
        pickle.dump(obj, f)


@public
def write_file(path, contents, mode='w', encoding=constants.ENCODING):
    """
        Writes to a file.

        Parameters
        ----------
        path: the file path
        contents: the file contents
        mode: the file opening mode (defaults to 'w')
        encoding: the encoding (defaults to `constants.ENCODING`)

        Return
        ----------
        true if the number of written bytes if greater the zero; 
                false otherwise
    """
    params.check_not_null(path, contents)
    written = 0
    with open(path, mode, encoding=encoding) as fp:
        written = fp.write(contents)
    return (written > 0)

@public
def build_list(name: str, default: list) -> list:
    params.check_not_null(default)
    return default if name is None else [name]

@public
def get_doc(method):
    """
        Gets the first line of the documentation of a given method.

        Parameters
        ----------
        method: the method

        Return
        ----------
        `str` that represents the first line of documentation
    """
    first_line = '?'
    if (method.__doc__ is None):
        return first_line
    
    lines = method.__doc__.splitlines()
    non_empty_lines = list(filter(lambda line: len(line) > 0, [line.strip() for line in lines]))
    if (len(non_empty_lines) > 0):
        first_line = non_empty_lines[0]
    
    return first_line