# -*- coding:utf-8 -*-
from annotations import public

import os

@public
def check_not_null(*args):
    """
        Parameters
        ----------
        Return
        ----------        
    """    
    assert (len(args) > 0)
    for arg in args:
        assert (arg is not None) 


@public
def check_not_empty(*args):
    """
        Parameters
        ----------
        Return
        ----------        
    """    
    assert (len(args) > 0)
    for arg in args:
        check_not_null(arg)
        assert (len(arg) > 0) 


@public
def check(condition):
    """
        Parameters
        ----------
        Return
        ----------        
    """    
    check_not_null(condition)
    assert (condition)


@public
def exists(*args):
    """
        Parameters
        ----------
        Return
        ----------        
    """    
    assert (len(args) > 0)
    for arg in args:
        check_not_null(arg)
        assert (os.path.exists(arg))    