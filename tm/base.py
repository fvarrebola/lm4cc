# -*- coding:utf-8 -*-
from annotations import public, private, constructor
from constants import RANDOM_STATE
from params import check_not_null, check_not_empty
from config import get_csv_dir, get_models_dir
import logger
import utils
import db

from functools import lru_cache
from progressbar import ProgressBar

from javalang import tokenizer as Tokenizer
from javalang.parser import Parser

import funcy as fp
import numpy as np
import os
import pandas as pd
import pyLDAvis
import spacy
from sklearn.feature_extraction import text 
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, ENGLISH_STOP_WORDS
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.model_selection import GridSearchCV


import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)

#region private_functions
@private
@lru_cache(maxsize=1)
def _get_nlp():
    return spacy.load('en_core_web_sm', disable=['parser', 'ner'])

def _row_norm(dists):
    return dists / dists.sum(axis=1)[:, None]

@private
def _extract_data_for_pyldavis(model, tf_dtm, tf_vectorizer):

    check_not_null(model, tf_dtm, tf_vectorizer)
    vocab = tf_vectorizer.get_feature_names()
    doc_lengths = tf_dtm.sum(axis=1).getA1()
    term_freqs = tf_dtm.sum(axis=0).getA1()
    topic_term_dists = _row_norm(model.components_)
    
    assert (term_freqs.shape[0] == len(vocab))
    assert (topic_term_dists.shape[1] == tf_dtm.shape[1])

    doc_topic_dists = _row_norm(model.transform(tf_dtm))
    
    return {'vocab': vocab,
            'doc_lengths': doc_lengths.tolist(),
            'term_frequency': term_freqs.tolist(),
            'doc_topic_dists': doc_topic_dists.tolist(),
            'topic_term_dists': topic_term_dists.tolist()}

@private
def _load_sentence_from_db(id):
    return db.get_sentence_for_topic_modeling_by_id(id).data

@private
def _normalize(nlp, sentence, prune_min, prune_max):
    
    check_not_null(nlp, sentence)

    tokens = [token.lemma_ for token in nlp(sentence) if token.lemma_ not in ['-PRON-']]
    if prune_min:
        tokens = list(filter(lambda x: len(x) >= prune_min, tokens))
    if prune_max:
        tokens = list(filter(lambda x: len(x) <= prune_max, tokens))
    return ' '.join(tokens)

@private
def _print_build_summary(params={}, sparsity=0.0, score=0.0, ppl=0.0):
    logger.info("\tBuild summary")
    logger.info("\t{}".format("-"*41))
    logger.info("\tparameters    = %s" % params)
    logger.info("\tsparsity      = %15.8f" % (sparsity * 100))
    logger.info("\tlog2prob      = %15.8f" % score)
    logger.info("\tperplexity    = %15.8f" % ppl)
#endregion

@public
class TopicModel(object):

    _LDA_BASE_OPTS = {
        'learning_method': 'batch',
        'learning_offset': 50.0,
        'random_state': RANDOM_STATE
    }

    _GRID_SEARCH_N_TOPICS = [3, 4, 5, 6, 7, 10]
    _GRID_SEARCH_LEARNING_DECAYS = [0.5, 0.7, 0.9]
    _GRID_SEARCH_MAX_ITERS = [5, 10]
    _GRID_SEARCH_PARAMS = {
        'n_topics': _GRID_SEARCH_N_TOPICS, 
        'learning_method': ['batch'],
        'learning_decay': _GRID_SEARCH_LEARNING_DECAYS, 
        'learning_offset': [50.0],
        'random_state': [RANDOM_STATE],
        'max_iter': _GRID_SEARCH_MAX_ITERS
    }

    @private
    def _parse_kwargs(self, kwargs):
        self._kwargs = kwargs
        self._print_summaries = kwargs.get('print_summaries', True)
        self._show_progress_bar = kwargs.get('show_progress_bar', False)
        self._prune_min = kwargs.get('prun_min', 5)
        self._prune_max = kwargs.get('prun_min', 0)
        self._min_df = 2
        self._max_df = 0.95
        self._max_features = kwargs.get('max_features', 20)
        self._n_topics = kwargs.get('topics', 5)
        self._save_pyldavis = kwargs.get('save_pyldavis', True)
        self._save_td_as_csv = kwargs.get('save_td_as_csv', False)

    @constructor
    def __init__(self, args, **kwargs):
        self._model = None
        self._parse_kwargs(kwargs)
    
    @private
    def _build_path(self, basepath: str = get_models_dir(), pre: str = None, pos: str = None) -> str:
        name = ('%s%s%s' % (('%s-' % pre if pre else ''), type(self).__name__, ('.%s' % pos if pos else '')))
        return os.path.join(basepath, name.lower())
    
    @private
    def _save_grid_search_results(self, grid):
        
        grid_results = []
        columns = ['n_topics', 'max_iter', 'learning_decay', 'score']
        for n_topics in self._GRID_SEARCH_N_TOPICS:
            for max_iter in self._GRID_SEARCH_MAX_ITERS:
                for learning_decay in self._GRID_SEARCH_LEARNING_DECAYS:
                    grid_results.append([n_topics, max_iter, learning_decay] + \
                        [s.mean_validation_score for s in grid.grid_scores_ \
                            if s.parameters['max_iter'] == max_iter and s.parameters['learning_decay'] == learning_decay and s.parameters['n_topics'] == n_topics])
        
        path = self._build_path(basepath=get_csv_dir(), pre='grid-search-results', pos='csv')
        logger.info("Saving grid search results to file '%s'..." % path)
        pd.DataFrame(grid_results, columns=columns).to_csv(path, sep='\t')


    @private
    def _create_pyldavis_panel(self, **kwargs):
        opts = _extract_data_for_pyldavis(self._model, self._dtm_tf, self._tf_vectorizer)
        return pyLDAvis.prepare(**fp.merge(opts, kwargs))

    @public
    def build(self, documents: list, grid_search: bool = False):

        check_not_empty(documents)
        total = len(documents)

        # normalizes input documents
        logger.info("Normalizing %d entries..." % total)
        if self._show_progress_bar:
            bar = ProgressBar(max_value=total, redirect_stdout=True)
            fn_update = bar.update
        
        nlp = _get_nlp()

        sentences = []
        self._document_names = []
        for (_, entry) in enumerate(documents):
            self._document_names.append(str(entry))
            sentences.append(_normalize(nlp, _load_sentence_from_db(entry), self._prune_min, self._prune_max))
            if self._show_progress_bar:
                fn_update(bar.value + 1)
        
        if self._show_progress_bar:
            bar.finish()
        
        # builds the frequency tables
        logger.info("Building frequency tables...")
        reserved = {'java', 'javax', 'com', 'sun', 'object', 'instance', 'method', 'property', 'type', 'value', 'string', 'exception', 'test'}
        keywords = Tokenizer.Keyword.VALUES.union(Tokenizer.Boolean.VALUES.union({'null'}))
        stop_words = ENGLISH_STOP_WORDS.union(reserved).union(keywords)

        self._tfidf_vectorizer = TfidfVectorizer(\
                    min_df=self._min_df, max_df=self._max_df, \
                    max_features=self._max_features, lowercase=True, \
                    stop_words=stop_words)
        self._tfidf = self._tfidf_vectorizer.fit_transform(sentences)
        
        self._tf_vectorizer = CountVectorizer(\
                    min_df=self._min_df, max_df=self._max_df, \
                    max_features=self._max_features, lowercase=True, \
                    stop_words=stop_words)
        self._dtm_tf  = self._tf_vectorizer.fit_transform(sentences)
        #tf_feature_names = tf_vectorizer.get_feature_names()
        
        # builds the final model
        if grid_search:
            logger.info("Doing grid search with params %s..." % self._GRID_SEARCH_PARAMS.items())
            lda = LatentDirichletAllocation(self._LDA_BASE_OPTS)
            grid = GridSearchCV(lda, param_grid=self._GRID_SEARCH_PARAMS)
            grid.fit(self._dtm_tf)
            self._save_grid_search_results(grid)
            self._model = grid.best_estimator_
        else:
            opts = fp.merge(self._LDA_BASE_OPTS, {'n_topics': self._n_topics, 'max_iter': 5 })
            logger.info("Creating default LDA model with params %s..." % opts.items())
            lda = LatentDirichletAllocation(**opts)
            lda.fit(self._dtm_tf)
            self._model = lda

        self._topic_names = [str(i) for i in range(1, self._model.n_topics + 1)]
        
        # builds the topic x keywords table
        self._df_topics_vs_keywords = pd.DataFrame(self._model.components_)
        self._df_topics_vs_keywords.columns = self._tf_vectorizer.get_feature_names()
        self._df_topics_vs_keywords.index = self._topic_names

        # builds the document x topic table
        lda_output = self._model.transform(self._dtm_tf)
        self._df_docs_vs_topics = pd.DataFrame(np.round(lda_output, 2), columns=self._topic_names, index=self._document_names)
        self._df_docs_vs_topics['dominant'] = np.argmax(self._df_docs_vs_topics.values, axis=1) + 1

        # print summaries
        if self._print_summaries:
            data_dense = self._dtm_tf.todense()
            sparsity = ((data_dense > 0).sum() / data_dense.size)
            _print_build_summary(\
                self._model.get_params(), \
                sparsity,
                self._model.score(self._dtm_tf), \
                self._model.perplexity(self._dtm_tf))
        
    @public
    def load(self, prefix: str = None):
        return utils.load_from_file(self._build_path(pre=prefix))
    
    @public
    def save(self, prefix: str = None):

        utils.save_to_file(self, self._build_path(pre=prefix))

        if self._save_td_as_csv:
            self._df_docs_vs_topics.to_csv(\
                self._build_path(basepath=get_csv_dir(), pre='%s-topic-distribution' % prefix, pos='csv'), sep='\t')

        # builds and saves the pyLDAvis panel
        if self._save_pyldavis:
            pyLDAvis.save_html(\
                self._create_pyldavis_panel(), \
                self._build_path(basepath=get_csv_dir(), pre='%s-pyldavis' % prefix, pos='html'))

    
    @public
    def predict(self, sentence) -> tuple:
        """
            Predicts the topic for a given sentence.
        """
        check_not_empty(sentence)

        nlp = _get_nlp()

        sentence = [_normalize(nlp, sentence, self._prune_min, self._prune_max)]
        scores = self._model.transform(self._tf_vectorizer.transform(sentence))
        
        index = np.argmax(scores)
        topic = self._df_topics_vs_keywords.iloc[index,:]
        
        return (topic, scores.item(index))
    
    @public
    def tf_vectorizer(self):
        """
            Returns the count vectorizer.
        """
        return self._tf_vectorizer
    
    @public
    def dtm_tf(self):
        """
            Returns the document term-matrix.
        """
        return self._dtm_tf
    
    @public
    def topic_vs_keywords_table(self):
        """
            Returns the topic x keywords table.
        """
        return self._df_topics_vs_keywords

    @public
    def doc_vs_topic_table(self):
        """
            Returns the docment x topic table.
        """
        return self._df_docs_vs_topics
    
    @public
    def docs_vs_topics_dominant_table(self):
        """
            Returns the docment x topic table grouped by dominant topic.
        """
        return self._df_docs_vs_topics.groupby('dominant')