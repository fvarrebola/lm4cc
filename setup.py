from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules=cythonize(["annotations.pyx", "constants.pyx", "params.pyx", "config.pyx", "logger.pyx", "args.pyx", "utils.pyx", "db.pyx", "corpora.pyx", "vocabulary.pyx"]))
