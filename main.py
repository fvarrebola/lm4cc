# -*- coding:utf-8 -*-
#!/usr/bin/env python3
from annotations import public, private
import constants
from params import check_not_null, check
from config import get_models_dir, get_csv_dir

import logger
import db
from corpora import load_sentences
from utils import get_doc
from lm.base import NGram
from lm.addone import AddOneSmoothing
from lm.interpolated import InterpolatedWithConditionalLambdas
from lm.backoff import NaiveBackOff, HeuristicBackOff
from lm.kn import KneserNey

from tm.base import TopicModel

import argparse
from collections import OrderedDict
import fnmatch
from funcy import merge
from operator import itemgetter
import os

import pandas as pd

PPL = 'ppl'
MRR = 'mrr'

LM_MODELS = { 
    'addone': AddOneSmoothing, 
    'backoff-standard': NaiveBackOff, 
    'backoff-with-heuristic': HeuristicBackOff, 
    'interpolated': InterpolatedWithConditionalLambdas, 
    'kneser-ney-standard': KneserNey}
    #'kneser-ney-modified': None }
LM_MODELS = OrderedDict(sorted(LM_MODELS.items(), key=itemgetter(0)))

#db._declare_all_classes()

#region private
@private
def __get_lm_class(lm_name: str):
    return LM_MODELS[lm_name] if lm_name else KneserNey

@private
def __compute_lm_class_range(args) -> []:
    lm_class_range = []
    if args.use_lm_variations:
        models = LM_MODELS.copy()
        _ = models.pop('backoff-standard')
        lm_class_range = models.values()
    else:
        lm_class_range = [__get_lm_class(args.lm)]
    return lm_class_range

@private
def __compute_order_range(args):
    (order, min_order, max_order) = (args.order, args.min_order, args.max_order)
    order_range = range(constants.DEFAULT_NGRAM_ORDER, constants.DEFAULT_NGRAM_ORDER + 1)
    if order:
        order_range = range(order, order + 1)
    elif min_order and max_order:
        order_range = range(\
            min(max(min_order, constants.MIN_NGRAM_ORDER), constants.MAX_NGRAM_ORDER), \
            min(max(min_order, max_order), constants.MAX_NGRAM_ORDER) + 1)
    return order_range

@private
def __compute_sentence_range(args, upper_limit: int) -> []:
    sentence_range = []
    if args.use_sentence_progression:
        digits = len(str(upper_limit))
        candidates = [(a * 10**d) for d in range(0, digits) for a in range(1,10)]
        sentence_range = list(filter(lambda x: x < upper_limit, candidates))
    sentence_range += [upper_limit]
    return sentence_range

@private
def __compute_user_range(args) -> []:
    user_range = []
    if args.user_name:
        user_range = [db.get_user(name=args.user_name)]
    else:
        user_range = db.get_user_list()
    return user_range

@private
def __parse_lm_params(args):
    return (__compute_lm_class_range(args), __compute_order_range(args))

@private
def __merge(kwargs, filename_prexix):
    return merge(kwargs, {'filename_prefix': filename_prexix})

@private
def __concat(*args):
    return '-'.join(filter(None, args))

@private
def _load_or_build_lm_model(lm_class, order, vocab, train_data, args, **kwargs):
    if args.load_model:
        model = lm_class(order, vocab, **kwargs).load()
    else:
        model = lm_class(order, vocab, **kwargs)
        model.build(train_data, held_out=args.held_out)
        if args.save_model:
            model.save()
    return model

@private
def _evaluate_language_model(lm, data, metric, args, **kwargs):
    
    KNOWN_METRICS = [PPL, MRR]
    check(metric in KNOWN_METRICS)
    
    (lm_class_range, order_range) = lm
    (train_data, test_data, vocab) = data
    
    prefix = kwargs.get('filename_prefix', None)
    sentence_range = __compute_sentence_range(args, upper_limit=len(train_data))

    if metric == PPL:
        columns = ['train', 'order', 'ppl', 'ce', 'test', 'words', 'oov', 'zeroprobs', 'log2', 'log10', 'queries', 'time']
    if metric == MRR:
        columns = ['train', 'order', 'mrr', 'total', 'total_queries', 'min_mrr', 'max_mrr', 'avg_mrr', 'elapsed']
        
    for lm_class in lm_class_range:

        df = pd.DataFrame(columns=columns)
        lm_name = lm_class.__name__.lower()

        for order in order_range:
                
            for limit in sentence_range:
                if args.load_model:
                    logger.info("Loading '%s' LM with n=%d from file '%s'..." % (lm_name, order, prefix))
                    model = lm_class(order, vocab, **kwargs).load(prefix)
                else:
                    logger.info("Building '%s' LM with n=%d from %d training samples..." % (lm_name, order, limit))
                    model = lm_class(order, vocab, **kwargs)
                    model.build(train_data[:limit], held_out=args.held_out)
                    if args.save_model:
                        model.save(prefix)
                
                logger.info("Evaluating '%s' over %d testing samples..." % (metric, len(test_data)))
                
                if metric == PPL:
                    measurement = model.perplexity(test_data, include_summary=True)
                if metric == MRR:
                    measurement = model.mrr(test_data, include_summary=True)
                
                df = df.append(pd.DataFrame(data=([[limit, order, *pd.Series(measurement)]]), columns=columns))
        
        path = os.path.join(get_csv_dir(), \
                ('%s%s-%s-n%d-%d.csv' % \
                    (('%s-' % prefix) if prefix else '', metric, lm_name, order_range.start, order_range.stop - 1)))
        
        logger.info("Saving evaluation results to file '%s'..." % path)
        df.to_csv(path, sep='\t', index=False)

#endregion

#region build_and_save_topic_models
@public
def build_prj_lms(args, **kwargs):
    
    check_not_null(args.filter_name)

    (lm_class_range, order_range) = __parse_lm_params(args)
    user_range = __compute_user_range(args)

    for user in user_range:
        for project in db.get_project_list(user_id=user.id):
            (train, _, vocab) = load_sentences(args.filter_name, project=project.name)
            if not train:
                continue
            for lm_class in lm_class_range:
                for order in order_range:
                    model = lm_class(order, vocab, **kwargs)
                    model.build(train, held_out=args.held_out)
                    model.save(project.name)

@public
def build_user_lms(args, **kwargs):
    
    check_not_null(args.filter_name)

    (lm_class_range, order_range) = __parse_lm_params(args)
    user_range = __compute_user_range(args)
    
    for user in user_range:
        (train, _, vocab) = load_sentences(args.filter_name, user=user.name)
        if not train:
            continue
        for lm_class in lm_class_range:
            for order in order_range:
                model = lm_class(order, vocab, **kwargs)
                model.build(train, held_out=args.held_out)
                model.save(user.name)

@public
def build_user_tms(args, **kwargs):
    
    check_not_null(args.filter_name)
    
    for user in __compute_user_range(args):
        (train, _, _) = load_sentences(args.filter_name, user=user.name)
        model = TopicModel(args, **kwargs)
        model.build(train, grid_search=args.grid_search)
        model.save('%s-tm' % user.name)

@public
def build_global_tms(args, **kwargs):
    
    check_not_null(args.filter_name)
    
    (train, _, _) = load_sentences(args.filter_name)
    model = TopicModel(args, **kwargs)
    model.build(train, grid_search=args.grid_search)
    model.save('global-tm')


@public
def build_user_tm_lms(args, **kwargs):
    
    check_not_null(args.filter_name)

    (lm_class_range, order_range) = __parse_lm_params(args)
    user_range = __compute_user_range(args)
    
    for user in user_range:
        
        (tm_train, _, vocab) = load_sentences(args.filter_name, user=user.name)
        if not tm_train: 
            continue
        
        tm_model = TopicModel(args, **kwargs)
        tm_model.build(tm_train, grid_search=args.grid_search)
        tm_model.save('%s-tm' % user.name)

        table = tm_model.docs_vs_topics_dominant_table()
        for (grp_name, grp_data) in table.groups.items():
            for lm_class in lm_class_range:
                lm_train = list(map(int, grp_data.values.tolist()))
                for order in order_range:
                    model = lm_class(order, vocab, **kwargs)
                    model.build(lm_train, held_out=args.held_out)
                    model.save('%s-lm-tm-%s' % (user.name, grp_name))

@public
def build_global_tm_lms(args, **kwargs):
    
    check_not_null(args.filter_name)

    (lm_class_range, order_range) = __parse_lm_params(args)
    (tm_train, _, vocab) = load_sentences(args.filter_name)

    tm_model = TopicModel(args, **kwargs)
    tm_model.build(tm_train, grid_search=args.grid_search)
    tm_model.save('global-tm')

    table = tm_model.docs_vs_topics_dominant_table()
    for (grp_name, grp_data) in table.groups.items():
        lm_train = list(map(int, grp_data.values.tolist()))
        for lm_class in lm_class_range:
            for order in order_range:
                model = lm_class(order, vocab, **kwargs)
                model.build(lm_train, held_out=args.held_out)
                model.save('global-lm-tm-%s' % grp_name)

@public
def build_global_lm(args, **kwargs):

    check_not_null(args.filter_name)

    (lm_class_range, order_range) = __parse_lm_params(args)
    (train, _, vocab) = load_sentences(args.filter_name)
    for lm_class in lm_class_range:
        for order in order_range:
            model = lm_class(order, vocab, **kwargs)
            model.build(train, held_out=args.held_out)
            model.save('global')
#endregion

#region global_language_model_evaluations
@private
def __eval_global_lm(args, metric=PPL, **kwargs):

    check_not_null(args.filter_name)
    _evaluate_language_model(__parse_lm_params(args), load_sentences(args.filter_name), metric, args, **__merge(kwargs, 'global'))
    
@public
def eval_global_ppl(args, **kwargs):
    """
        Evaluates perplexity all projects using the standard 75%-25% split.
    """
    __eval_global_lm(args, **kwargs)

@public
def eval_global_mrr(args, **kwargs):
    """
        Evaluates MRR all projects using the standard 75%-25% split.
    """
    __eval_global_lm(args, metric=MRR, **kwargs)
#endregion

#region per_project_topic_language_model_evaluations
@public
def eval_prj_ppl_with_tm(args, **kwargs):

    check_not_null(args.filter_name, args.project_name)

    (train, test, vocab) = load_sentences(args.filter_name, project=args.project_name)
    
    (lm_class, _) = __parse_lm_params(args)
    prj_model = _load_or_build_lm_model(lm_class, 3, vocab, train, args, **kwargs)

    models_per_topic = dict()

    tm = TopicModel(args, **kwargs)
    tm.build(train, grid_search=False)
    df = tm.docs_vs_topics_dominant_table()
    #for _order in order_range:
    kwargs['print_summaries'] = False
    kwargs['show_progress_bar'] = False
    for (k, v) in df.groups.items():
        _train = list(map(int, v.values.tolist()))
        models_per_topic[k] = _load_or_build_lm_model(lm_class, 3, vocab, _train, args, **kwargs)
    
    (log2_a, log10_a, words_a, zero_probs_a, queries_a) = (0.0, 0.0, 0, 0, 0)
    (log2_b, log10_b, words_b, zero_probs_b, queries_b) = (0.0, 0.0, 0, 0, 0)
    for entry in test:
        (topic, prob) = tm.predict(db.get_sentence_for_topic_modeling_by_id(entry).data)
        try:
            model = models_per_topic[int(topic.name)]
        except KeyError:
            model = prj_model
        (l2, l10, (w, zp, q)) = model.sentence_log_probability(entry)
        (log2_a, log10_a, words_a, zero_probs_a, queries_a) = \
            (log2_a + l2, log10_a + l10, words_a + w, zero_probs_a + zp, queries_a + q)
    
        (l2, l10, (w, zp, q)) = prj_model.sentence_log_probability(entry)
        (log2_b, log10_b, words_b, zero_probs_b, queries_b) = \
            (log2_b + l2, log10_b + l10, words_b + w, zero_probs_b + zp, queries_b + q)
    
    prj_model.perplexity(test)
    
    print(-(log2_a / words_a), (log2_a, log10_a, words_a, zero_probs_a, queries_a))
    print(-(log2_b / words_b), (log2_b, log10_b, words_b, zero_probs_b, queries_b))
    #_evaluate_language_model(lm_class, order_range, vocab, train, test, ppl, mrr, args, **kwargs)

#endregion

#region project_language_model_evaluations
@private
def __eval_prj_lm(args, metric=PPL, **kwargs):

    check_not_null(args.filter_name)
    check(args.project_name or args.user_name)

    _evaluate_language_model(
            __parse_lm_params(args), 
            load_sentences(args.filter_name, project=args.project_name, user=args.user_name), 
            metric, args, **__merge(kwargs, __concat(args.project_name, args.user_name)))
    
@public
def eval_prj_ppl(args, **kwargs):
    """
        Evaluates perplexity of a single project using the standard 75%-25% split.
    """
    __eval_prj_lm(args, **kwargs)

@public
def eval_prj_mrr(args, **kwargs):
    """
        Evaluates MRR of a single project using the standard 75%-25% split.
    """
    __eval_prj_lm(args, metric=MRR, **kwargs)

@private
def __eval_prj_pair_lm(args, metric=PPL, **kwargs):

    check_not_null(args.filter_name)
    check((args.project_a_name or args.user_a_name) and \
            (args.project_b_name or args.user_b_name))

    (a_train, a_test, vocab) = load_sentences(args.filter_name, \
            project=args.project_a_name, user=args.user_a_name)
    (b_train, b_test, _) = load_sentences(args.filter_name, \
            project=args.project_b_name, user=args.user_b_name)
    (train, test) = (a_train + a_test, b_train + b_test)

    _evaluate_language_model(
            __parse_lm_params(args), 
            (train, test, vocab), 
            metric, args, **__merge(kwargs, __concat('pair', args.project_a_name, args.user_a_name, args.project_b_name, args.user_b_name)))

@public
def eval_prj_pair_ppl(args, **kwargs):
    """
        Evaluates perplexity a project pair.
        A language model is built from project A and evaluated against project B.
    """
    __eval_prj_pair_lm(args, **kwargs)

@public
def eval_prj_pair_mrr(args, **kwargs):
    """
        Evaluates MRR of a project pair.
        A language model is built from project A and evaluated against project B.
    """
    __eval_prj_pair_lm(args, metric=MRR, **kwargs)

@private
def __eval_cross_lm(args, metric=PPL, **kwargs):
    
    check_not_null(args.filter_name)
    check(args.project_name or args.user_name)

    (a_train, a_test, vocab) = load_sentences(args.filter_name, \
            project=args.project_name,  user=args.user_name)
    (b_train, b_test, _) = load_sentences(args.filter_name, \
            project=args.project_name,  user=args.user_name, negate_filters=True)
    (train, test) = (a_train + a_test, b_train + b_test)
    
    _evaluate_language_model(
            __parse_lm_params(args), 
            (train, test, vocab), 
            metric, args, **__merge(kwargs, __concat('cross', args.project_name, args.user_name)))

@public
def eval_cross_ppl(args, **kwargs):
    """ 
        Evaluates cross-project perplexity.
        A language model is built from a single project and it is evaluated against all remaining projects one at time.
    """
    __eval_cross_lm(args, **kwargs)

@public
def eval_cross_mrr(args, **kwargs):
    """ 
        Evaluates cross-project MRR.
        A language model is built from a single project and it is evaluated against all remaining projects one at time.
    """
    __eval_cross_lm(args, metric=MRR, **kwargs)

@private
def __eval_leave_one_out_lm(args, metric=PPL, **kwargs):

    check_not_null(args.filter_name)
    check(args.project_name or args.user_name)

    (a_train, a_test, vocab) = load_sentences(args.filter_name, \
            project=args.project_name, user=args.user_name, negate_filters=True)
    (b_train, b_test, _) = load_sentences(args.filter_name, \
            project=args.project_name, user=args.user_name)
    (train, test) = (a_train + a_test, b_train + b_test)
    
    _evaluate_language_model(__parse_lm_params(args),
            (train, test, vocab), 
            metric, args, **__merge(kwargs, __concat('leave-one-out', args.project_name, args.user_name)))

@public
def eval_leave_one_out_ppl(args, **kwargs):
    """
        Evaluates perplexity of a single project.
        A language model is built from all remaining projects.
    """
    __eval_leave_one_out_lm(args, **kwargs)

@public
def eval_leave_one_out_mrr(args, **kwargs):
    """
        Evaluates MRR of a single project.
        A language model is built from all remaining projects.
    """
    __eval_leave_one_out_lm(args, metric=MRR, **kwargs)
#endregion

#region 
@private
def __list(models_dir: str = get_models_dir(), pattern: str = '*'):
    return fnmatch.filter(os.listdir(models_dir), pattern)

@private
def __load_lm_models_from_tm(name, order, lm_class, vocab, **kwargs):
    models = dict()
    for f in __list(pattern='%s-lm-tm-*-n%d.bin' % (name, order)):
        sub = '%s-lm-tm-' % name
        key = f.split(sub)[1].split('-')[0]
        instance = lm_class(order, vocab, **kwargs).load('%s%s' % (sub, key))
        instance.__complete_vocabulary = vocab
        models[key] = instance
    return models

@private
def __unpack_to_series(data):
    (log2, log10, (words, zeroprobs, queries)) = data
    return pd.Series([log2, log10, words, zeroprobs, queries])

@public
def complete_eval(args, **kwargs):

    check_not_null(args.filter_name)
    
    (lm_class_range, order_range) = __parse_lm_params(args)
    user_range = __compute_user_range(args)
    
    _, _, vocab = load_sentences(args.filter_name)
    
    columns = ['project', 'user', 'order', 'sentence', \
        'project_log2', 'project_log10', 'project_words', 'project_zp', 'project_queries',\
        'user_log2', 'user_log10', 'user_words', 'user_zp', 'user_queries',\
        'user_topic', 'user_tm_log2', 'user_tm_log10', 'user_tm_words', 'user_tm_zp', 'user_tm_queries',\
        'global_topic', 'global_tm_log2', 'global_tm_log10', 'global_tm_words', 'global_tm_zp', 'global_tm_queries',\
        'global_log2', 'global_log10', 'global_words', 'global_zp', 'global_queries']

    for lm_class in lm_class_range:
        
        for order in order_range:
            
            global_lm = lm_class(order, vocab, **kwargs).load('global')
            global_tm = TopicModel(args, **kwargs).load('global-tm')
            global_tm_lms = __load_lm_models_from_tm('global', order, lm_class, vocab, **kwargs)
            
            for user in user_range:
                
                df = pd.DataFrame(columns=columns)

                user_lm = lm_class(order, vocab, **kwargs).load(user.name)
                user_tm = TopicModel(args, **kwargs).load('%s-tm' % user.name)
                user_tm_lms = __load_lm_models_from_tm(user.name, order, lm_class, vocab, **kwargs)
                
                for project in db.get_project_list(user_id=user.id):
                    
                    (train, test, _) = load_sentences(args.filter_name, project=project.name)
                    if not len(train) or not len(test):
                        continue
                    
                    prj_lm = lm_class(order, vocab, **kwargs).load(project.name)
                    
                    for entry in test:
                        
                        sentence = db.get_sentence_for_topic_modeling_by_id(entry).data
                        if not sentence:
                            sentence = ' '

                        (usr_tm, _), (glb_tm, _) = \
                                user_tm.predict(sentence), global_tm.predict(sentence)

                        prj_m, usr_m, usr_tm_m, glb_tm_m, glb_m = \
                                prj_lm.sentence_log_probability(entry), \
                                user_lm.sentence_log_probability(entry), \
                                user_tm_lms[usr_tm.name].sentence_log_probability(entry), \
                                global_tm_lms[glb_tm.name].sentence_log_probability(entry), \
                                global_lm.sentence_log_probability(entry)

                        df = df.append(pd.DataFrame(data=([[\
                                project.name, user.name, order, entry, \
                                *__unpack_to_series(prj_m), \
                                *__unpack_to_series(usr_m), \
                                usr_tm.name, *__unpack_to_series(usr_tm_m), \
                                glb_tm.name, *__unpack_to_series(glb_tm_m), \
                                *__unpack_to_series(glb_m)]]), columns=columns))
    
                path = os.path.join(get_csv_dir(), '%s-complete-eval.csv' % user.name)
                logger.info("Saving evaluation results to file '%s'..." % path)
                df.to_csv(path, sep='\t', index=False)

#endregion

#region fun
def have_a_little_fun(args, **kwargs):

    (lm_class, _) = __parse_lm_params(args)
    (train_data, test_data, vocab) = load_sentences(args.filter_name, user=args.user_name, project=args.project_name)
    
    bos = vocab.word2id(constants.BOS)
    ngram = []
    if (args.order > 1):
        ngram = [bos]* (args.order - 1)
    
    output = []
    if args.words:
        word_split = args.words.split(',')
        ngram = [vocab.word2id(s) for s in word_split]
        output = word_split

    if args.user_name:
        filename = args.user_name
    elif args.project_name:
        filename = args.project_name
    else:
        filename = 'global'

    lm = lm_class(args.order, vocab, **kwargs).load(filename)
    idx = 0
    while idx < 100:
        candidates = lm.candidates(ngram, id2word=False)
        if len(candidates):
            top1 = candidates[0][0]
            w = vocab.id2word(top1)
            output += [w]
            ngram = ngram[1:] + [top1]
        idx += 1

    print(output)

#endregion

#region main
@public
def main(args: argparse.Namespace):
    """
    """
    check_not_null(args)

    if args.use_sentence_progression:
        args.debug_sentences = args.debug_ngrams = False

    kwargs = {}
    kwargs['prune_min'] = args.prune_min
    kwargs['prune_max'] = args.prune_max
    kwargs['print_summaries'] = args.print_summaries
    kwargs['show_progress_bar'] = args.show_progress_bar
    kwargs['debug_sentences'] = args.debug_sentences
    kwargs['debug_ngrams'] = args.debug_ngrams
    kwargs['max_features'] = args.max_features
    kwargs['topics'] = args.topics

    if (args.build_prj_lms):
        build_prj_lms(args, **kwargs)
    elif (args.build_user_lms):
        build_user_lms(args, **kwargs)
    elif (args.build_user_tms):
        build_user_tms(args, **kwargs)
    elif (args.build_user_tm_lms):
        build_user_tm_lms(args, **kwargs)
    elif (args.build_global_lm):
        build_global_lm(args, **kwargs)
    elif (args.build_global_tms):
        build_global_tms(args, **kwargs)
    elif (args.build_global_tm_lms):
        build_global_tm_lms(args, **kwargs)
    elif (args.ppl):
        eval_global_ppl(args, **kwargs)
    elif (args.prj_ppl):
        eval_prj_ppl(args, **kwargs)
    elif (args.prj_pair_ppl):
        eval_prj_pair_ppl(args, **kwargs)
    elif (args.cross_ppl):
        eval_cross_ppl(args, **kwargs)
    elif (args.leave_one_out_ppl):
        eval_leave_one_out_ppl(args, **kwargs)
    elif (args.mrr):
        eval_global_mrr(args, **kwargs)
    elif (args.prj_mrr):
        eval_prj_mrr(args, **kwargs)
    elif (args.prj_pair_mrr):
        eval_prj_pair_mrr(args, **kwargs)
    elif (args.cross_mrr):
        eval_cross_mrr(args, **kwargs)
    elif (args.leave_one_out_mrr):
        eval_leave_one_out_mrr(args, **kwargs)
    elif (args.topic_ppl):
        eval_prj_ppl_with_tm(args, **kwargs)
    elif (args.complete_eval):
        complete_eval(args, **kwargs)
    elif (args.have_fun):
        have_a_little_fun(args, **kwargs)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(\
        description='This script helps you perfom LM/MRR evaluations.',\
        #usage='python main.py <command> [<args>]',\
        formatter_class=argparse.RawTextHelpFormatter)

    """parser.add_argument('command', help='Subcommand to execute')
    module = sys.modules[__name__]
    args = parser.parse_args(sys.argv[1:2])
    if not hasattr(module, args.command):
        parser.print_help()
        exit(1)
    getattr(module, args.command)()"""

    group = parser.add_mutually_exclusive_group(required=True)
    
    # model building
    group.add_argument('-build-prj-lms', action='store_true', help=get_doc(build_prj_lms))
    group.add_argument('-build-user-lms', action='store_true', help=get_doc(build_user_lms))
    group.add_argument('-build-user-tms', action='store_true', help=get_doc(build_user_tms))
    group.add_argument('-build-user-tm-lms', action='store_true', help=get_doc(build_user_tm_lms))
    group.add_argument('-build-global-lm', action='store_true', help=get_doc(build_global_lm))
    group.add_argument('-build-global-tms', action='store_true', help=get_doc(build_global_tms))
    group.add_argument('-build-global-tm-lms', action='store_true', help=get_doc(build_global_tm_lms))
    
    # perplexity evaluations
    group.add_argument('-ppl', action='store_true', help=get_doc(eval_global_ppl))
    group.add_argument('-prj-ppl', action='store_true', help=get_doc(eval_prj_ppl))
    group.add_argument('-prj-pair-ppl', action='store_true', help=get_doc(eval_prj_pair_ppl))
    group.add_argument('-cross-ppl', action='store_true', help=get_doc(eval_cross_ppl))
    group.add_argument('-leave-one-out-ppl', action='store_true', help=get_doc(eval_leave_one_out_ppl))

    # mrr evaluations
    group.add_argument('-mrr', action='store_true', help=get_doc(eval_global_mrr))
    group.add_argument('-prj-mrr', action='store_true', help=get_doc(eval_prj_mrr))
    group.add_argument('-prj-pair-mrr', action='store_true', help=get_doc(eval_prj_pair_mrr))
    group.add_argument('-cross-mrr', action='store_true', help=get_doc(eval_cross_mrr))
    group.add_argument('-leave-one-out-mrr', action='store_true', help=get_doc(eval_leave_one_out_mrr))

    group.add_argument('-topic-ppl', action='store_true')
    group.add_argument('-complete-eval', action='store_true')
    group.add_argument('-have_fun', action='store_true')

    # optional arguments
    parser.add_argument('--filter-name', choices=constants.FILTER_NAMES, default=constants.FILTER_NONE)
    
    parser.add_argument('--user-name', type=str, default=None)
    parser.add_argument('--user-a-name', type=str, default=None)
    parser.add_argument('--user-b-name', type=str, default=None)

    parser.add_argument('--project-name', type=str, default=None)
    parser.add_argument('--project-a-name', type=str, default=None)
    parser.add_argument('--project-b-name', type=str, default=None)
    
    parser.add_argument('--lm', choices=LM_MODELS.keys())
    parser.add_argument('--order', type=int, default=None)
    parser.add_argument('--held-out', action='store_true', default=False)
    parser.add_argument('--min-order', type=int, default=None)
    parser.add_argument('--max-order', type=int, default=None)
    parser.add_argument('--prune-min', type=int, default=None)
    parser.add_argument('--prune-max', type=int, default=None)
    
    parser.add_argument('--grid-search', action='store_true', default=False)
    parser.add_argument('--topics', type=int, default=5)
    parser.add_argument('--max-features', type=int, default=10)

    parser.add_argument('--use-lm-variations', action='store_true', default=False)
    parser.add_argument('--use-sentence-progression', action='store_true', default=False)
    parser.add_argument('--print-summaries', action='store_true', default=False)
    parser.add_argument('--show-progress-bar', action='store_true', default=False)
    parser.add_argument('--debug-sentences', action='store_true', default=False)
    parser.add_argument('--debug-ngrams', action='store_true', default=False)
    parser.add_argument('--load-model', action='store_true', default=False)
    parser.add_argument('--save-model', action='store_true', default=False)
    parser.add_argument('--words', type=str, default=None)

    main(parser.parse_args())
#endregion