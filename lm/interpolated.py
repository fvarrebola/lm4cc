# -*- coding:utf-8 -*-
from annotations import public, constructor, override
from constants import RANDOM_STATE, LRU_CACHE_MAX_SIZE
from vocabulary import Vocabulary
from .base import NGram

from functools import lru_cache
from sklearn.model_selection import train_test_split

DEFAULT_GAMMA = 50.0
FACTOR = 0.1

@public
class InterpolatedWithConditionalLambdas(NGram):
    """
        Interpolated ngram with lambdas conditional on context and add-one smoothing.
        
        Remarks
        ----------
        Conditional probability is given by:

    """
    @constructor
    def __init__(self, N: int, V: Vocabulary, gamma: float = DEFAULT_GAMMA, add_one: bool = True, **kwargs):
        super().__init__(N, V, **kwargs)
        self._gamma = gamma
        self._add_one = add_one
    
    @override
    def _do_build_with_held_out(self, training_set: list):
        (train, test) = train_test_split(training_set, train_size=0.9, test_size=0.1, random_state=RANDOM_STATE)
        results = list()
        avg = round(self.V() / self.S())
        gammas = [avg + (i * avg) for i in range(10)]
        for gamma in gammas:
            self._gamma = gamma
            self._do_build(train)
            results.append((gamma, self.perplexity(test)))
        results.sort(key=lambda x: x[1])
        self._gamma = results[0][0]

    @override
    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def _do_conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):

        fn_cnt = self.count

        # first, we compute conditional lambdas
        lambdas = []
        for i in range(0, self._order - 1):
            cnt = fn_cnt(previous_tokens[i : self._order - 1])
            lambdas.append((1 - sum(lambdas)) * (cnt / (cnt + self._gamma)))
        lambdas.append(1 - sum(lambdas))
        prob = 0.0

        # then, computes ngram counts
        _range = range(0, self._order)
        for i in _range:
            result = 0.0
            
            previous = previous_tokens[i:]
            cnt = fn_cnt(tuple(previous) + (token,))
            aux = float(fn_cnt(previous)) if len(previous) else self.W_plus_paddings()
            if self._add_one and not len(previous):
                result = (cnt + 1) / (aux + self.V())
            elif aux:
                result = cnt / aux
            prob += result * lambdas[i]
        
        return prob

    @override
    def get_extra_params_str(self):
        return 'gamma = %.2f, add_one = %d' % (self._gamma, self._add_one)