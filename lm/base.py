# -*- coding:utf-8 -*-
from annotations import public, private, protected, constructor, override, deprecated

from constants import ENCODING, MAX_NGRAM_ORDER, TYPE_GLOBAL, BOS, EOS, LOWER_THRESHOLD, UPPER_THRESHOLD, MAX_CANDIDATES, LRU_CACHE_MAX_SIZE
import logger
import params
from config import get_models_dir
import utils
import corpora
import db
import vocabulary

from ast import literal_eval
from collections import defaultdict
from math import log
from functools import lru_cache
from marisa_trie import RecordTrie # pylint: disable=E0611
from numpy import prod
from random import random
from tempfile import gettempdir

import os
import pickle
from progressbar import ProgressBar
import time

@private
def _load_trie(name):
    """
    Loads a trie from the database using the given `name`.

    Parameters
    ----------
    name: the trie name


    Return
    ----------
    the unpickled data trie
    """
    params.check_not_empty(name)
    return pickle.loads(db.get_trie(name).data)


@deprecated 
def __load_sentence(id):
    """
    Loads a sentence from the database using the given `id`.

    Parameters
    ----------
    id: the sentence id


    Return
    ----------
    the unpickled sentence
    """    
    return pickle.loads(db.get_sentence_by_id(id).data)


@private
def _map_sentence_using_trie_vocabulary(sentence, vocabulary):
    """
    """
    params.check_not_null(sentence)
    params.check(type(sentence) == list and type(sentence[0]) == str)

    fn_w2id = vocabulary.word2id
    return list(map(lambda word: fn_w2id(word), sentence))


@private
def _load_sentence_from_db(id):
    """
    Loads a sentence from the database using the given `id`.

    Parameters
    ----------
    id: sentence id

    Return
    ----------
    sentence
    """    
    params.check_not_null(id)
    return literal_eval(db.get_sentence_by_id(id).data)


@private
def _pad_sentence(sentence, ngram_order, lpad, rpad):
    """
        Pads a sentence for the given ngram order.

        Paramaters
        ----------
        sentence: input sentece
        ngram_order: n-gram order
        lpad: left pad symbol
        rpad: right pad symbol

        Return
        ----------
        the padded sentence
    """
    params.check_not_empty(sentence)
    params.check(ngram_order > 0)

    if (sentence[0] != lpad):
        sentence = [lpad]*(ngram_order - 1) + sentence + [rpad]

    return sentence

@public
def int_tuple_to_str(data):
    return ''.join(map(lambda x: '%12d' % x, data))

@private
def _build_trie(counts: dict) -> dict:
    """
        Builds prefix tries out of ngram counts.

        Parameters
        ----------
        counts: dict of ngram counts

        Return
        ----------
        dict of prefix tries
    """
    tries = dict()
    for (o, count) in counts.items():
        keys = []
        values = []
        for (k, v) in count.items():
            keys.append(int_tuple_to_str(k))
            values.append((v,))
        tries[o] = RecordTrie('<L', zip(keys, values))
    return tries
    #return RecordTrie('<L', zip(keys, values))


@public
class NotImplementedException(Exception):
    def __init__(self):
        super().__init__('Not implemented.')

@public
class NGram(object):
    """
        Ngram base class.

        Remarks
        ----------
        Ngram counts are built as data tries (`marisa_trie.RecordTrie`). 
    """
    @private
    def _print_build_summary(self, sents=0, avg_len=0, words=0, vocab_len=0, ngrams=0):
        p_vocab_len = self._partial_vocabulary_len
        c_vocab_len = len(self._complete_vocabulary)
        logger.info("\tBuild summary")
        logger.info("\t{}".format("-"*41))
        logger.info("\tsents         = %d" % sents)
        logger.info("\tavg sent len  = %.2f" % avg_len)
        logger.info("\twords (p.v.)  = %d (%d)" % (words, p_vocab_len))
        logger.info("\twords (c.v.)  = %d (%d)" % (words, c_vocab_len))
        for o in range(self._order, 0, -1):
            logger.info("\t%2d ngrams     = %d" % (o, len(ngrams[o])))

    @private
    def _print_sentence_header(self):
        logger.info("\t{:^12} {:^12} {:^12} {:^4} {:^15} {:^15}".format("id", "words", "ngrams", "zp", "logprob (2)", "logprob (10)"))
        logger.info("\t{} {} {} {} {} {}".format("-"*12, "-"*12, "-"*12, "-"*4, "-"*15, "-"*15))

    @private
    def _print_sentence_summary(self, idx=0, words=0, ngrams=0, zero_probs=0, log2prob=0.0, log10prob=0.0):
        logger.info("\t{:12d} {:12d} {:12d} {:4d} {:15.8f} {:15.8f}".format(idx, words, ngrams, zero_probs, log2prob, log10prob))

    @private
    def _print_conditional_probability_summary(self, token: int, previous_tokens: tuple, prob=0.0):
        logger.info("\t\tP(%+8s | %+16s) = %.8f [%.8f] [%.8f]" % (token, previous_tokens, prob, log(prob, 2), log(prob, 10)))

    @private
    def _print_cross_entropy_summary(self, sents=0, words=0, oov=0, zero_probs=0, log2prob=0.0, log10prob=0.0, queries=0, elapsed=0):
        p_vocab_len = self._partial_vocabulary_len
        c_vocab_len = len(self._complete_vocabulary)
        cross_entropy = -(log2prob / words)
        logger.info("\tPPL evalution summary")
        logger.info("\t{}".format("-"*75))
        logger.info("\torder         = %d" % self._order)
        logger.info("\textra params  = %s" % self.get_extra_params_str())
        logger.info("\tsents         = %d" % sents)
        logger.info("\twords (p.v.)  = %d (%d)" % (words, p_vocab_len))
        logger.info("\twords (c.v.)  = %d (%d)" % (words, c_vocab_len))
        logger.info("\toovs          = %d" % oov)
        logger.info("\tzero-probs    = %d" % zero_probs)
        logger.info("\tprob (log2)   = %.8f" % log2prob)
        logger.info("\tcross entropy = %.8f" % cross_entropy)
        logger.info("\tperplexity    = %.8f" % pow(2, cross_entropy))
        logger.info("\ttps           = %.2f" % (queries / elapsed))
        logger.info("\tSRILM logprob = %.8f" % log10prob)
        logger.info("\tSRILM ppl1    = %.8f" % pow(10, - (log10prob / (sents + words - oov)))) 
        logger.info("\tSRILM ppl2    = %.8f" % pow(10, - (log10prob / (words - oov))))
    
    @private
    def _print_sentence_mrr_header(self):
        logger.info("\t{:^12} {:^12} {:^15}".format("id", "queries", "mrr"))
        logger.info("\t{} {} {}".format("-"*12, "-"*12, "-"*15))

    @private
    def _print_sentence_mrr_summary(self, idx=0, queries=0, mrr=0.0):
        logger.info("\t{:12d} {:12d} {:15.8f}".format(idx, queries, mrr))

    @private
    def _print_mrr_summary(self, sents=0, queries=0, min_mrr=0.0, max_mrr=0.0, avg_mrr=0.0, elapsed=0):
        logger.info("\tMRR evalution summary")
        logger.info("\t{}".format("-"*41))
        logger.info("\torder         = %d" % self._order)
        logger.info("\textra params  = %s" % self.get_extra_params_str())
        logger.info("\tsents         = %d" % sents)
        logger.info("\tqueries       = %d" % queries)
        logger.info("\tmin (avg)     = %.8f" % min_mrr)
        logger.info("\tmax (avg)     = %.8f" % max_mrr)
        logger.info("\tmrr (avg)     = %.8f" % avg_mrr)

    @private
    def _parse_kwargs(self, kwargs):
        
        self._kwargs = kwargs
        self._compute_bo_counts = kwargs.get('compute_bo_counts', False)
        self._compute_kn_counts = kwargs.get('compute_kn_counts', False)
        
        self._token_threshold = kwargs.get('token_threshold', None)
        self._prune_min = kwargs.get('prune_min', None)
        self._prune_max = kwargs.get('prune_max', None)
        self._print_summaries = kwargs.get('print_summaries', True)
        self._show_progress_bar = kwargs.get('show_progress_bar', False)
        self._debug_sentences = kwargs.get('debug_sentences', False)
        self._debug_ngrams = kwargs.get('debug_ngrams', False)
        if self._debug_sentences or self._debug_ngrams: 
            self._show_progress_bar = False
        
    @constructor
    def __init__(self, N, V, **kwargs):
        """
            Class constructor.

            Paramateres
            ----------
            order: ngram order
            vocabulary: complete vocabulary
            kwargs:
                token_threshold: max number of tokens to consider during training
                lower_ngram_count_threshold: exclusive lower threshold for ngram counts
                upper_ngram_count_threshold: exclusive upper threshold for ngram counts
        """
        params.check(0 < N <= MAX_NGRAM_ORDER)
        params.check_not_null(V)
        params.check(type(V) == vocabulary.Vocabulary)

        self._order = N
        self._complete_vocabulary = V
        self._partial_vocabulary = set()
        self._partial_vocabulary_len = 0
        self._counts = dict()

        self._LPAD_SYMBOL = self._complete_vocabulary.word2id(BOS)
        self._RPAD_SYMBOL =  self._complete_vocabulary.word2id(EOS)

        self._alpha = self._beta = self._gamma = self._delta = 0.0
        self._parse_kwargs(kwargs)

    @private
    def _build_path(self, pre: str = None, pos: int = None):
        name = ('%s%s-n%d%s' % (('%s-' % pre if pre else ''), type(self).__name__, self._order, ('.%d' % pos if pos else '')))
        return os.path.join(get_models_dir(), name.lower())

    @protected
    def _do_build(self, training_set: list):
        """
            Builds a ngram model.
        """
        entry = training_set[0]
        load_from_db = (type(entry) == int)
        load_from_trie = (type(entry) == list and type(entry[0]) == str)
        ignore = (type(entry) == list and type(entry[0]) == int)
        params.check(load_from_db or load_from_trie or ignore)

        KN_CONTINUATION_COUNT_KEYS = ['dot_tokens', 'tokens_dot', 'dot_tokens_dot']

        _range = range(1, self._order + 1)
        ng_counts = dict()
        if self._compute_bo_counts:
            self._A = dict()
            self._A_counts = dict()
        if self._compute_kn_counts:
            self._kn_N = dict()
            self._kn_N_counts = dict()
            for k in KN_CONTINUATION_COUNT_KEYS:
                self._kn_N[k] = dict()
                self._kn_N_counts[k] = dict()
            self._kn_N['dot_dot'] = 0
        
        for o in _range:
            ng_counts[o] = defaultdict(int)
            if self._compute_bo_counts:
                self._A[o] = defaultdict(set)
                self._A_counts[o] = defaultdict(int)
            if self._compute_kn_counts:
                for k in KN_CONTINUATION_COUNT_KEYS:
                    self._kn_N[k][o] = defaultdict(set)
                    self._kn_N_counts[k][o] = defaultdict(int)

        overall_token_cnt = 0
        self._sentence_count = len(training_set)
        total = self._token_threshold if self._token_threshold else self._sentence_count

        if self._show_progress_bar:
            bar = ProgressBar(max_value=total, redirect_stdout=True)
            fn_update_bar = bar.update
    
        self._word_count = 0
        self._word_count_including_paddings = 0
        fn_add_to_partial_vocab = self._partial_vocabulary.add
        for (_, sentence) in enumerate(training_set):
            
            if self._token_threshold and overall_token_cnt == self._token_threshold:
                break

            # loads the training sentence
            if (load_from_db):
                sentence = _load_sentence_from_db(sentence) 
            elif (load_from_trie):
                sentence = _map_sentence_using_trie_vocabulary(sentence, self._complete_vocabulary)
            
            # checks whether sentence is empty
            sentence_len = len(sentence)
            if (sentence_len == 0): 
                raise Exception("Sentences should not be empty.")
            self._word_count += sentence_len

            # checks if counters are within threshold bounds 
            step = 1
            if self._token_threshold:
                if (overall_token_cnt + sentence_len > self._token_threshold):
                    step = self._token_threshold - overall_token_cnt
                    sentence[:step]
                    overall_token_cnt = self._token_threshold
                else:
                    step = sentence_len
                    overall_token_cnt += sentence_len

            # pads the input sentence
            sentence = _pad_sentence(sentence, self._order, self._LPAD_SYMBOL, self._RPAD_SYMBOL)
            self._word_count_including_paddings += len(sentence)

            # updates the partial vocabulary
            _ = [fn_add_to_partial_vocab(word) for word in sentence]
            
            # computes all ngrams
            for o in _range:
                for ng in list(zip(*[sentence[idx:] for idx in range(o)])):
                    ng_counts[o][ng] += 1
                    if self._compute_bo_counts:
                        self._A[o][ng[:-1]].add(ng[-1])
                    if self._compute_kn_counts:
                        (r, l, r_k, l_k, m_k) = (ng[-1:], ng[:1], ng[1:], ng[:-1], ng[1:-1])
                        self._kn_N['dot_tokens'][o][r_k].add(l)
                        self._kn_N['tokens_dot'][o][l_k].add(r)
                        if m_k:
                            self._kn_N['dot_tokens_dot'][o][m_k].add(r)
                            self._kn_N['dot_tokens_dot'][o][m_k].add(l)
            
            if self._show_progress_bar:
                fn_update_bar(bar.value + step)

        if self._show_progress_bar:
            bar.finish()

        # prunes ngram counts
        if self._prune_min and self._prune_min > LOWER_THRESHOLD:
            for o in _range:
                ng_counts[o] = dict(filter(lambda x: x[1] > self._prune_min, ng_counts[o].items()))
        
        if self._prune_max and self._prune_max > UPPER_THRESHOLD:
            for o in _range:
                ng_counts[o] = dict(filter(lambda x: x[1] < self._prune_max, ng_counts[o].items()))

        self._average_sentence_len = self._word_count / float(self._sentence_count)
        self._partial_vocabulary_len = len(self._partial_vocabulary)
        
        logger.debug("Building data tries...")
        self._counts = _build_trie(ng_counts)
        
        if self._compute_bo_counts:
            logger.debug("Building BackOff counts...")
            for o in _range:
                for (ng, a_set) in self._A[o].items():
                    tng = tuple(ng)
                    self._A_counts[o][ng] += sum([((ng_counts[o][tng + (w,)] - self._delta) / float(ng_counts[o - 1][tng])) if o > 1 else self.V() for w in a_set])
            self._seen_words = set(map(int, self._counts[1].keys()))

        if self._compute_kn_counts:
            logger.debug("Building KneserNey continuation counts...")
            for o in _range:
                for key in KN_CONTINUATION_COUNT_KEYS:
                    for (k, v) in self._kn_N[key][o].items():
                        self._kn_N_counts[key][o][k] = len(v) 
            if self._order > 1:
                self._kn_N['dot_dot'] = sum([len(v) for v in self._kn_N['dot_tokens'][2].values()])
            self._kn_N['dot_tokens'] = None
            self._kn_N['tokens_dot'] = None
            self._kn_N['dot_tokens_dot'] = None

    @protected
    def _do_build_with_held_out(self, training_set: list):
        """
            Builds a ngram model using a held-out set.
        """
        raise NotImplementedException()

    @public
    def build(self, training_set: list, held_out: bool = False):
        """
            Builds a model using the given `training_set`.

            Parameters
            ----------
            training_set: training sentences
            held_out: whether to build a model using a held-out function

            Remarks
            ----------
            Training sentences can be a list of ints or a list of strings. If they are set to a list of ints, each int will represent a sentence id that will be used for loading the actual sentence from a database. On the other hand, if sentences are set to a list of strings, each term will be mapped to a corresponding int value using the vocabulary trie.

            If `held_out` is `True` then we call `self._build_with_held_out()` function.
        """
        params.check_not_empty(training_set)

        logger.debug("Building %d-gram %s params %s" % (self._order, ('with held-out and' if held_out else 'with'), self._kwargs))

        if held_out:
            if len(training_set) < 10:
                logger.warn("Training set is too small for building a LM with held-out!")
                logger.info("Using standard build...")
                self._do_build(training_set)
            else:
                self._do_build_with_held_out(training_set)
        else:
            self._do_build(training_set)
        
        if self._print_summaries:
            self._print_build_summary(sents=self._sentence_count, avg_len=self._average_sentence_len,   words=self._word_count, ngrams=self._counts)
        #if (self.use_cnt_trie):
        #_save_trie(self._build_ngram_name(), self.counts)
        #else:
        #_save_ngram_counts_csv(self.type, self.order, counts)

    @public
    def load(self, prefix: str = None, V = None):
        """
            Loads a ngram model.
        """
        #if (self.use_cnt_trie):
        #for k_order in range(self._order, 0, -1):
        #    self._counts[k_order] = utils.load_from_file(self._build_output_file(filename_prefix, k_order))
        model = utils.load_from_file(self._build_path(prefix))
        if V and type(V) == vocabulary.Vocabulary:
            model._complete_vocabulary = V
        return model


    @public
    def save(self, prefix: str = None, include_vocabulary: bool = False):
        """
            Saves a ngram model.
        """
        #for k_order in range(self._order, 0, -1):
        #    utils.save_to_file(self._counts[k_order], self._build_output_file(filename_prefix, k_order))
        if not include_vocabulary:
            self._complete_vocabulary = None
        utils.save_to_file(self, self._build_path(prefix))

    @public
    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def count(self, ngram):
        """
            Returns the count of the given ngram.
        
            If the given `ngram` dimensions are lower than the current ngram 
            order, counts are computed using the given `ngram` as a prefix.

            Parameters
            ----------
            ngram: `tuple` that represents the ngram

            Return
            ----------
            `int` that represent the `ngram` count
        """
        params.check_not_null(ngram)
        
        order = len(ngram) if type(ngram) == tuple else 0
        params.check(order <= self._order)

        cnt = 0.0
        if not order: 
            return cnt
        
        if (type(ngram[0]) == str):
            fn_w2id = self._complete_vocabulary.word2id
            ngram = tuple(map(lambda x: fn_w2id(x), ngram))
            if None in ngram: 
                return cnt

        key = int_tuple_to_str(ngram)
        try:
            hit = self._counts[order][key]
            if len(hit):
                cnt = hit[0][0]
        except:
            pass
        
        return cnt

    @protected
    def _do_conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):
        """
            Computes the conditional propability of a `token` given a `tuple` of
            `previous_tokens`.

            Parameters
            ----------
            token: current token
            previous_tokens: previous tokens

            Returns
            ----------
            conditional probability of `token` given `previous_tokens`

            Remarks
            ----------
            This method must be overriden by `NGram` descendants.
        """
        prob = 0.0
        ngram = previous_tokens + (token,)
        aux = self.count(previous_tokens)
        if (aux > 0):
            prob = self.count(ngram) / float(aux)
        return prob

    @public
    def conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):
        """
            Computes the conditional propability of a `token` given a `tuple` of
            `previous_tokens`.

            Parameters
            ----------
            token: current token
            previous_tokens: previous tokens
            kwargs: extra dict arguments

            Returns
            ----------
            conditional probability of `token` given `previous_tokens`
        """
        params.check_not_null(token)
        params.check(len(previous_tokens) < self._order)

        prob = self._do_conditional_probability(token, previous_tokens, **kwargs)
        if self._debug_ngrams:
            self._print_conditional_probability_summary(token, previous_tokens, prob)

        #if prob > 1.0:
        #    raise Exception("\n!!! Please review your model !!!\nInvalid probability found at P(%s | %s) = %f." % (token, previous_tokens, prob))

        return prob

    @public
    def sentence_probability(self, sentence):
        """
            Computes the propability of a `sentence` as the product of the 
            conditional probability of all its underlying ngrams.

            Parameters
            ----------
            sentence: input sentence

            Returns
            ----------
            sentence probability
        """
        params.check_not_null(sentence)

        should_load_from_db = (type(sentence) == int)
        should_map_using_trie = (type(sentence) == list and type(sentence[0]) == str)
        params.check(should_load_from_db or should_map_using_trie)
        
        if (should_load_from_db):
            sentence = _load_sentence_from_db(sentence) 
        else:
            sentence = _map_sentence_using_trie_vocabulary(sentence, self._complete_vocabulary)

        if (len(sentence) == 0): 
            raise ValueError()
        
        sentence = _pad_sentence(sentence, self._order, self._LPAD_SYMBOL, self._RPAD_SYMBOL)

        fn_cp = self.conditional_probability
        prob = 1.0
        ngrams = list(zip(*[sentence[idx:] for idx in range(self._order)]))
        for ng in ngrams:
            prob *= fn_cp(ng[-1], ng[:self._order - 1])
        
        return prob

    @public
    def sentence_log_probability(self, sentence):
        """
            Computes the  `log` probability of a `sentence` as the sum of the conditional probability of all its underlying ngrams.

            Parameters
            ----------
            sentence: input sentence`

            Returns
            ----------
            sentence log probability
        """
        params.check_not_null(sentence)

        load_from_db = (type(sentence) == int)
        map_using_trie = (type(sentence) == list and type(sentence[0]) == str)
        ignore = (type(sentence) == list and type(sentence[0]) == int)
        params.check(load_from_db or map_using_trie or ignore)
        
        if load_from_db:
            sentence = _load_sentence_from_db(sentence) 
        elif map_using_trie:
            sentence = _map_sentence_using_trie_vocabulary(sentence, self._complete_vocabulary)

        if (len(sentence) == 0): 
            raise ValueError()

        words = len(sentence)
        sentence = _pad_sentence(sentence, self._order, self._LPAD_SYMBOL, self._RPAD_SYMBOL)
        
        prob = log2 = log10 = 0.0
        zero_probs = queries = 0

        fn_cp = self.conditional_probability
        ngrams = list(zip(*[sentence[idx:] for idx in range(self._order)]))
        for ng in ngrams:
            token = ng[-1]
            previous_tokens = ng[:self._order - 1]
            prob = fn_cp(token, previous_tokens)
            queries += 1
            if prob:
                log2 += log(prob, 2)
                log10 += log(prob, 10)
            else:
                zero_probs += 1
        
        return (log2, log10, (words, zero_probs, queries))

    @public 
    def cross_entropy(self, sentences, include_summary=False):
        """
            Computes the cross-entropy of all given `sentences`.

            Parameters
            ----------
            sentences: test sentences

            Returns
            ----------
            cross entropy
        """
        params.check_not_null(sentences)

        log2 = log10 = 0.0
        words = zero_probs = queries = 0

        total = len(sentences)
        
        logger.debug("Evaluating cross entropy for %s sentences..." % total)

        if self._show_progress_bar:
            bar = ProgressBar(max_value=total, redirect_stdout=True)
            fn_update_bar = bar.update
        
        start = time.time()

        if self._debug_sentences:
            self._print_sentence_header()

        fn_slp = self.sentence_log_probability
        for (idx, sentence) in enumerate(sentences):
            (l2, l10, (w, zp, q)) = fn_slp(sentence)
            (log2, log10, words, zero_probs, queries) = \
                (log2 + l2, log10 + l10, words + w, zero_probs + zp, queries + q)
            if self._debug_sentences:
                self._print_sentence_summary(idx=idx + 1, words=w, ngrams=q, zero_probs=zp, log2prob=l2, log10prob=l10)

            if self._show_progress_bar:
                fn_update_bar(bar.value + 1)
        
        if self._show_progress_bar:
            bar.finish()

        elapsed = (time.time() - start)
        cross_entropy = -(log2 / words)
        
        if self._print_summaries:
            self._print_cross_entropy_summary(sents=total, words=words, oov=0, zero_probs=zero_probs, log2prob=log2, log10prob=log10, queries=queries, elapsed=elapsed)

        if include_summary:
            return (cross_entropy, total, words, 0, zero_probs, log2, log10, queries, elapsed)
        else:
            return cross_entropy

    @public
    def perplexity(self, sentences, include_summary=False):
        """
            Computes the perplexity of all given `sentences`.

            Parameters
            ----------
            sentences: test sentences

            Returns
            ----------
            perplexity
        """
        params.check_not_null(sentences)
        data = self.cross_entropy(sentences, include_summary)
        perplexity = pow(2, data[0] if include_summary else data)
        if include_summary:
            return (perplexity, *data)
        else:
            return perplexity
        
    @public
    def candidates(self, tokens, limit: int = MAX_CANDIDATES, id2word: bool = True) -> tuple:
        """
            Returns a list of candidate terms based on the given parameters.
        
            Parameters
            ----------
            tokens: history tokens
            limit: max number of candidates

            Return
            ----------
            2-tuple of candidate terms, incluiding the term itself and the assigned probability
        """
        params.check(0 < limit <= MAX_CANDIDATES)
        
        tokens = tuple(tokens)
        tokens_len = len(tokens)
        params.check(tokens_len < self._order)

        fn_w2id = self._complete_vocabulary.word2id
        fn_id2w = self._complete_vocabulary.id2word

        if (tokens_len > 0 and type(tokens[0]) == str):
            tokens = tuple(map(lambda x: fn_w2id(x), tokens))
        
        candidates = list()
        key = int_tuple_to_str(tokens)
        for hit in self._counts[tokens_len + 1].keys(key):
            word_id = int(hit[12 * tokens_len:].strip())
            candidates.append((fn_id2w(word_id) if id2word else word_id, self.conditional_probability(word_id, tokens)))

        return sorted(candidates, key=lambda k: k[1], reverse=True)[:limit]

    @private
    def _mrr_rank(self, history, correct_response):
        """
            Computes the Mean Reciprocal Rank of 
        """
        params.check_not_null(history, correct_response)

        rank = 0.0
        index = -1
        for (_index, (candidate, _)) in enumerate(self.candidates(history, id2word = False)):
            if correct_response == candidate:
                index = _index
                break
        if (index > -1):
            rank = 1 / (index + 1)
        return rank

    @public
    def mrr(self, sentences, include_summary=False):
        """
            Computes the Mean Reciprocal Rank of a set of sentences.
        """
        params.check_not_null(sentences)

        total = len(sentences)
        
        logger.debug("Evaluating MRR for %s sentences..." % total)
        if self._show_progress_bar:
            bar = ProgressBar(max_value=total, redirect_stdout=True)
            fn_update_bar = bar.update

        start = time.time()

        if self._debug_sentences:
            self._print_sentence_mrr_header()
        
        total_queries = 0
        min_mrr = 1.0
        max_mrr = 0.0
        total_mrr = 0.0
        for (idx, sentence) in enumerate(sentences):
            
            sentence = _load_sentence_from_db(sentence) 
            if (len(sentence) == 0): 
                raise ValueError()

            sentence = _pad_sentence(sentence, self._order, self._LPAD_SYMBOL, self._RPAD_SYMBOL)
            ngrams = list(zip(*[sentence[idx:] for idx in range(self._order)]))
            
            queries = len(ngrams)
            total_queries += queries
            
            mrr = sum([self._mrr_rank(ng[:-1], ng[-1]) for ng in ngrams]) / queries
            min_mrr = min(min_mrr, mrr)
            max_mrr = max(max_mrr, mrr)
            total_mrr += mrr
            
            if self._debug_sentences:
                self._print_sentence_mrr_summary(idx=idx + 1, queries=queries, mrr=mrr)
            
            if self._show_progress_bar:
                fn_update_bar(bar.value + 1)
        
        if self._show_progress_bar:
            bar.finish()

        elapsed = (time.time() - start)

        avg_mrr = (total_mrr / total)
        if self._print_summaries:
            self._print_mrr_summary(sents=total, queries=total_queries, min_mrr=min_mrr, max_mrr=max_mrr, avg_mrr=avg_mrr, elapsed=elapsed)

        if include_summary:
            return (mrr, total, total_queries, min_mrr, max_mrr, avg_mrr, elapsed)
        else:
            return mrr

    @public 
    def W(self):    
        """ 
            Returns the overall word count seen during training set.

            Returns
            ----------
            `int` representing the overall word count
        """
        return self._word_count 

    @public 
    def W_plus_paddings(self):    
        """ 
            Returns the overall word count (including paddings) seen during training set.

            Returns
            ----------
            `int` representing the overall word count
        """
        return self._word_count_including_paddings

    @public
    def S(self):
        """ 
            Returns the overall sentence count seen during training.

            Returns
            ----------
            `int` representing the overall sentence count
        """
        return self._sentence_count

    @public
    def V(self):
        """
            Returns the length of the vocabulary.

            Returns
            ----------
            `int` representing the vocabulary length
        """
        return self._partial_vocabulary_len
    
    @public
    def get_extra_params_str(self):
        return None