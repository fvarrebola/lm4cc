# -*- coding:utf-8 -*-
from annotations import public, private, constructor, override
from constants import RANDOM_STATE

import vocabulary
from constants import RANDOM_STATE, LRU_CACHE_MAX_SIZE
from .base import NGram

from functools import lru_cache
from sklearn.model_selection import train_test_split

DEFAULT_BETA = 0.5
FACTOR = 0.1

#region navie_backoff
@public
class NaiveBackOff(NGram):
    """
        Remarks
        ----------
        The naive approach will take a long time to compute and it could exceeded the maximum recursion depth.
    """
    @constructor
    def __init__(self, N: int, V: vocabulary.Vocabulary, delta: float = DEFAULT_BETA, add_one: bool = True, **kwargs):
        super().__init__(N, V, **kwargs)
        self._delta = delta
        self._add_one = add_one
        self._compute_bo_counts = True

    @override
    def _do_build_with_held_out(self, training_set: list):
        (train, test) = train_test_split(training_set, train_size=0.9, test_size=0.1, random_state=RANDOM_STATE)
        results = list()
        deltas = [round(i * FACTOR, 2) for i in range(1, 10)]
        for delta in deltas:
            self._delta = delta
            self._do_build(train)
            results.append((delta, self.perplexity(test)))
        results.sort(key=lambda x: x[1])
        self._delta = results[0][0]

    @override
    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def _do_conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):
        
        prob = 0.0
        
        order = len(previous_tokens) + 1

        fn_cnt = self.count
        if previous_tokens:
            prev_cnt = fn_cnt(previous_tokens)
            if token in self._A[order][previous_tokens]:
                prob = (fn_cnt(tuple(previous_tokens) + (token,)) - self._delta) / prev_cnt
            else:
                _q_bo = self.conditional_probability(token, previous_tokens[1:])
                _alpha = 1 - self._A_counts[order][previous_tokens]
                B = self._seen_words - self._A[order][previous_tokens]
                _b_sum = sum([self.conditional_probability(b, previous_tokens[1:]) for b in B])
                _denom = _b_sum if _b_sum else len(self._counts[order]) 
                prob = (_alpha * _q_bo) / float(_denom)
        else:
            cnt = fn_cnt(token)
            prob = ((cnt + 1) / (cnt + self.V())) if self._add_one else (cnt / self.W_plus_paddings())
        
        return prob

    @override
    def get_extra_params_str(self):
        return 'delta = %.2f, add_one = %d' % (self._delta, self._add_one)
#endregion

#region heuristic_backoff 
@public
class HeuristicBackOff(NGram):
    @constructor
    def __init__(self, N: int, V: vocabulary.Vocabulary, delta: float = DEFAULT_BETA, add_one: bool = True, **kwargs):
        super().__init__(N, V, **kwargs)
        self._delta = delta
        self._add_one = add_one
        self._compute_bo_counts = True

    @override
    def _do_build_with_held_out(self, training_set: list):
        (train, test) = train_test_split(training_set, train_size=0.9, test_size=0.1)
        results = list()
        deltas = [round(i * FACTOR, 2) for i in range(1, 10)]
        for delta in deltas:
            self._delta = delta
            self._do_build(train)
            results.append((delta, self.perplexity(test)))
        results.sort(key=lambda x: x[1])
        self._delta = results[0][0]

    @override
    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def _do_conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):
        
        prob = 0.0
    
        history = tuple(previous_tokens)
        order = len(previous_tokens) + 1

        fn_cnt = self.count
        if previous_tokens:
            prev_cnt = fn_cnt(history)
            if token in self._A[order][history]:
                prob = (fn_cnt(history + (token,)) - self._delta) / prev_cnt
            else:
                q_bo = self.conditional_probability(token, history[1:])
                A = self._A[order][history]
                denom = 1 - sum([self.conditional_probability(w, history[1:]) for w in A])
                if denom:
                    A_len = len(A)
                    alpha = self._delta * A_len / self.count(history) if A_len else 1
                    prob = alpha * q_bo / denom
        else:
            cnt = fn_cnt(token)
            prob = ((cnt + 1) / (cnt + self.V())) if self._add_one else (cnt / self.W_plus_paddings())
        
        return prob

    @override
    def get_extra_params_str(self):
        return 'delta = %.2f, add_one = %d' % (self._delta, self._add_one)
#endregion