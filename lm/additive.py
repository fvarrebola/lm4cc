# -*- coding:utf-8 -*-
from annotations import public, constructor, override
from constants import RANDOM_STATE
from vocabulary import Vocabulary
from .base import NGram 
from sklearn.model_selection import train_test_split

DEFAULT_DELTA = 0.5
FACTOR = 0.1

@public
class AdditiveNGram(NGram):
    """
        Additive ngram class.
    """
    @constructor
    def __init__(self, N: int, V: Vocabulary, delta: float = DEFAULT_DELTA, **kwargs):
        super().__init__(N, V, **kwargs)
        self._delta = delta

    @override
    def _do_build_with_held_out(self, training_set: list):
        (train, test) = train_test_split(training_set, train_size=0.9, test_size=0.1, random_state=RANDOM_STATE)
        results = list()
        deltas = [round(i * FACTOR, 2) for i in range(1, 11)]
        for delta in deltas:
            self._delta = delta
            self._do_build(train)
            results.append((delta, self.perplexity(test)))
        results.sort(key=lambda x: x[1])
        self._delta = results[0][0]

    @override
    def _do_conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):
        """
            Remarks
            ----------
            Conditional probability is given by

                 C(wi-n...wi) + delta
            _____________________________
             C(wi-n...wi-1) + (V * delta)
            
            where:
            - V is the vocabulary length (including EOS and excluding the BOS)
            - delta is some value between 0 and 1
        """
        fn_cnt = self.count
        return (self._delta + fn_cnt(previous_tokens + (token,))) / (float(fn_cnt(previous_tokens)) + (self._delta * self.V()))

    @override
    def get_extra_params_str(self):
        return 'delta = %.2f' % (self._delta)