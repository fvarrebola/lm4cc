# -*- coding:utf-8 -*-
from annotations import public, constructor, override
from constants import LRU_CACHE_MAX_SIZE
from vocabulary import Vocabulary
from .base import NGram 

from functools import lru_cache

@public
class AddOneSmoothing(NGram):
    """
        Add-one ngram class.

        Remarks
        ----------
        Conditional probability is given by

          C(wi-n...wi) + 1
        ____________________
         C(wi-n...wi-1) + V
        
        where:
        - V is the vocabulary length
        """
    @constructor
    def __init__(self, N: int, V: Vocabulary, **kwargs):
        super().__init__(N, V, **kwargs)

    @override
    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def _do_conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):
        history = tuple(previous_tokens)
        fn_cnt = self.count
        return (fn_cnt(history + (token,)) + 1) / (float(fn_cnt(history)) + self.V())