# -*- coding:utf-8 -*-
from annotations import public, private, constructor, override
from constants import RANDOM_STATE

import vocabulary
from constants import RANDOM_STATE, LRU_CACHE_MAX_SIZE
from .base import NGram

from functools import lru_cache
from sklearn.model_selection import train_test_split

DEFAULT_DELTA = 0.5
FACTOR = 0.1

@public
class KneserNey(NGram):
    @constructor
    def __init__(self, N: int, V: vocabulary.Vocabulary, delta: float = DEFAULT_DELTA, add_one: bool = True, **kwargs):
        super().__init__(N, V, **kwargs)
        self._delta = delta
        self._add_one = add_one
        self._compute_kn_counts = True


    @override
    def _do_build_with_held_out(self, training_set: list):
        (train, test) = train_test_split(training_set, train_size=0.9, test_size=0.1, random_state=RANDOM_STATE)
        results = list()
        deltas = [round(i * FACTOR, 2) for i in range(1, 10)]
        for delta in deltas:
            self._delta = delta
            self._do_build(train)
            results.append((delta, self.perplexity(test)))
        results.sort(key=lambda x: x[1])
        self._delta = results[0][0]


    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def continuation_count(self, ngram: tuple) -> int:
        return self._kn_N_counts['dot_tokens'][len(ngram) + 1][ngram]


    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def _compute_lambda(self, history: tuple) -> float:
        return self._delta * max(self._kn_N_counts['tokens_dot'][len(history) + 1][history], 1)

    @override
    @lru_cache(maxsize=LRU_CACHE_MAX_SIZE)
    def _do_conditional_probability(self, token: int, previous_tokens: tuple, **kwargs):
        
        history = tuple(previous_tokens)
        token_tpl = (token,)

        ngram = history + token_tpl
        order = len(ngram)

        fn_cnt = self.count 
        fn_cont_cnt = self.continuation_count

        if not history and self._order == 1:
            return (fn_cnt(token_tpl) + 1) / (self.W() + self.V())

        if not history and self._order > 1:
            return max(fn_cont_cnt(token_tpl), 1) / (self._kn_N['dot_dot'] + self.V())

        if len(ngram) == self._order:
            cnt = fn_cnt(history) + 1
            return (max(fn_cnt(ngram) - self._delta, 0) / cnt) + \
                ((self._compute_lambda(history) / cnt) * self.conditional_probability(token, history[1:]))
        else:
            cnt = max(self._kn_N_counts['dot_tokens_dot'][order + 1][history], 1)
            return  (max(fn_cont_cnt(ngram) - self._delta, 0) / cnt) + \
                (self._compute_lambda(history) / cnt) * (self.conditional_probability(token, history[1:]))


    @override
    def get_extra_params_str(self):
        return 'delta = %.2f, add_one = %d' % (self._delta, self._add_one)