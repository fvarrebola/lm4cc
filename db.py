# -*- coding:utf-8 -*-
# pylint: disable=E0602, E1120, E1123
from annotations import public, private

from constants import MIN_NGRAM_ORDER, MAX_NGRAM_ORDER, MAX_HITS, FILTER_NAMES
import config
import logger
import utils
import params

from collections import defaultdict
from inspect import getmembers, isclass
from sqlobject import BLOBCol, IntCol, SQLObject, StringCol, connectionForURI, sqlbuilder, sqlhub
from sys import modules

import pickle

# -----------------------------------------------------------------------------
MSG_INVALID_NGRAM = "Invalid ngram %s (%d records)"
# -----------------------------------------------------------------------------

import psycopg2
from functools import lru_cache

#region orm_classes
@private
def _declare_data_classes():
    """
        Declares ORM data classes.
    """
    DATA_CLASSES = (
        ("GitHubUser", {"name": StringCol()}),
        ("Project", {"user_id": IntCol(), "name" : StringCol(), "path" : StringCol()}),
        ("File", {"project_id" : IntCol(), "name" : StringCol(), "path" : StringCol(), "sha256": StringCol()}),
        ("Filter", {"name" : StringCol()}),
        ("Vocabulary", {"filter_id" : IntCol(), "word": StringCol()}),
        ("Sentence", {"filter_id" : IntCol(), "file_id" : IntCol(), "data" : StringCol()}),
        ("SentenceForTopicModeling", {"file_id" : IntCol(), "project_id": IntCol(), "data" : StringCol()}),
        ("SentenceFailure", {"filter_id" : IntCol(), "file_id" : IntCol()}),
        ("TrainTestSplit", {"filter_id" : IntCol(), "train_size": IntCol(), "train_data" : StringCol(), "test_size": IntCol(), "test_data": StringCol()}),
        ("TrainTestSplitPerProject", {"user_id": IntCol(), "project_id" : IntCol(), "filter_id" : IntCol(), "train_size": IntCol(), "train_data" : StringCol(), "test_size": IntCol(), "test_data": StringCol()}),

        ("Trie", {"name" : StringCol(), "data": BLOBCol()})
    )
    for (name, attrs) in DATA_CLASSES:
        setattr(modules[__name__], name, type(name, (SQLObject,), dict({ '__module__': __name__}, **attrs)))

@private
def _declare_ngram_classes():
    """
        Declares ORM ngram classes.

        Remarks
        ----------
        `Raw?NGram` classes point to all available ngrams.

        `N?Gram` classes point to pruned ngrams.

        `N^GramPerProject` classes point to ngrams of a given project.

        `N?GramPerTopic` classes point to ngram of a given topic.

    """
    TRAINING_NGRAM_CLASSES = (
        ("TrainingRawN%dGram", {"cnt" : IntCol()} ),
        ("TrainingN%dGram", {"cnt" : IntCol()} ),
        ("TrainingN%dGramPerProject", {"prj_id" : IntCol(), "cnt" : IntCol()}),
        ("TrainingN%dGramPerTopic", {"topic_id" : IntCol(), "cnt" : IntCol()}),
        ("ValidationN%dGram", {"sent_id": IntCol()})
    )
    for (name, attrs) in TRAINING_NGRAM_CLASSES:
        for order in range(MIN_NGRAM_ORDER, MAX_NGRAM_ORDER + 1):
            f_name = name % order
            std_attrs = {'__module__': __name__}
            for idx in range(1, order + 1): 
                std_attrs['w%d' % idx] = IntCol()
            setattr(modules[__name__], f_name, type(f_name, (SQLObject,), dict(std_attrs, **attrs)))

@private
def _declare_all_classes():
    """
        Declares ORM classes inside the current module.
    """
    _declare_data_classes()
    _declare_ngram_classes()

@private
def _get_all_orm_module_classes():
    """
        Returns the list of all ORM classes declared within the current module.
    """
    return getmembers(modules[__name__], lambda m: isclass(m) and m.__module__ == __name__)

@private
def _create_table_for_sqlobj(sql_obj, tx):
    """
        Creates a table for the given ORM class.
    """
    if not sql_obj.tableExists(connection=tx):
        sql_obj.createTable(connection=tx)

@private
def _create_tables(*sql_objs):
    """
        Creates tables for all ORM classes given.
    """
    tx = new_tx()
    for sql_obj in sql_objs:
        _create_table_for_sqlobj(sql_obj, tx)
    commit_tx(tx)

@private
def _drop_table_for_sqlobj(sql_obj, tx):
    """
        Drops a table for the given ORM class.
    """
    if sql_obj.tableExists(connection=tx) == 1:
        sql_obj.dropTable(connection=tx)

@private
def _drop_tables(*sql_objs):
    """
        Drops tables for all ORM classes given.
    """
    tx = new_tx()
    for sql_obj in sql_objs:
        _drop_table_for_sqlobj(sql_obj, tx)
    commit_tx(tx)

@public
def createdb():
    """
        Creates the database using all ORM classes declared within the current module.
    """
    _create_tables(_get_all_orm_module_classes())
    _create_default_filters()

@public
def dropdb():
    """
        Drops the database using all ORM classes declared within the current module.
    """
    _drop_tables(_get_all_orm_module_classes())

#endregion


#region ngram_query_cache
NGRAM_QUERY_CACHE = defaultdict(str)
@private 
def _initialize_cache():
    TABLES = { 
        "training" : {
            "global": "training_n%d_gram", 
            "per_project": "training_n%d_gram_per_project", 
            "per_topic": "training_n%d_gram_per_topic"
         },
         "validation": {
             "global": "validation_n%d_gram"
        }
    }
    for (train_or_test, mappings) in TABLES.items():
        for (tbl_type, table) in mappings.items():
            for f_order in range(MIN_NGRAM_ORDER, MAX_NGRAM_ORDER + 1):
                for s_order in range(f_order + 1):
                    sql_where = " and ".join(["w%d = %%s" % i for i in range(1, s_order + 1)])
                    sql = "SELECT sum(cnt) FROM %s%s" % ((table % f_order), ((" WHERE %s" % sql_where) if s_order else '')) 
                    NGRAM_QUERY_CACHE[(train_or_test, tbl_type, f_order, s_order)] = sql
#endregion

@private
def _new_connection(uri=config.get_sql_url(), debug=config.get_sql_debug()):
    params.check_not_null(uri)
    sqlhub.processConnection = connectionForURI(uri)
    sqlhub.processConnection.debug = debug
    return sqlhub.processConnection


@private
def _new_connection_if_needed():
    need_connection = True
    if (hasattr(sqlhub, 'processConnection') and sqlhub.processConnection is not None):
        need_connection = False
    if need_connection:
        _new_connection()


@private
def _create_default_filters():
    tx = new_tx()
    for f in FILTER_NAMES:
        Filter(name=f, connection=tx)
    commit_tx(tx)

@public
def get_filter_by_name(filter_name: str):
    params.check_not_null(filter_name)
    return select_one_by(Filter, name=filter_name)

@public
def get_sentences_by_filter_id(filter_id: int):
    return select_by(Sentence, filter_id=filter_id) 

@public
def new_tx():
    _new_connection_if_needed()
    return sqlhub.processConnection.transaction()


@public
def commit_tx(tx):
    tx.commit(close=True)


@public
def qty(sqlobj):
    _new_connection_if_needed()
    return sqlobj.select().count()


@public
def select(sqlobj):
    _new_connection_if_needed()
    return sqlobj.select()


@public
def select_by(sqlobj, **kwargs):
    _new_connection_if_needed()
    return sqlobj.selectBy(**kwargs)


@public
def select_one(sqlobj):
    return select(sqlobj).getOne()


@public
def select_one_by(sqlobj, **kwargs):
    return select_by(sqlobj, **kwargs).getOne()


@public
def add_trie(name, data, tx):
    return Trie(name=name, data=pickle.dumps(data), connection=tx)


@public
def add_project(project_name, project_path, tx):
    return Project(name=project_name, path=project_path, connection=tx)


@public
def add_project_file(prj_id, name, file_path, tx):
    return File(project_id=prj_id, name=name, path=file_path, connection=tx)


@public
def add_sentence(filter_id, file_id, sentence, tx):
    return Sentence(filter_id=filter_id, file_id=file_id, data=pickle.dumps(sentence), connection=tx)

@public
def add_train_test_split(**kwargs):
    tx = new_tx()
    TrainTestSplit(**kwargs, connection=tx)
    commit_tx(tx)

@public
def add_project_train_test_split(**kwargs):
    tx = new_tx()
    TrainTestSplitPerProject(**kwargs, connection=tx)
    commit_tx(tx)

@public
def add_sentence_failure(filter_id, file_id, tx):
    return SentenceFailure(filter_id=filter_id, file_id=file_id, connection=tx)

@public
def get_user(**kwargs):
    return select_one_by(GitHubUser, **kwargs)

@public
def get_user_list(**kwargs):
    return select_by(GitHubUser, **kwargs)

@public
def get_project_ids():
    _new_connection_if_needed()
    return list(map(lambda e: e.id, select(Project)))

@public
def get_project(**kwargs):
    return select_one_by(Project, **kwargs)

@public
def get_project_list(**kwargs):
    return select_by(Project, **kwargs)

@public
def get_file_ids(project_id=None):
    _new_connection_if_needed()
    return list(map(lambda e: e.id, select_by(File, project_id=project_id)))

@public
def get_sentence_by_id(sentence_id):
    params.check_not_null(sentence_id)
    _new_connection_if_needed()
    return select_by(Sentence, id=sentence_id).getOne()

@public
def get_sentence_ids(filter_id, project_id=None):
    params.check_not_null(filter_id)
    _new_connection_if_needed()
    where = ('filter_id = %d' % filter_id)
    if project_id:
        where += (' and file_id in (select id from file where project_id = %d)' % project_id)
    conn = sqlhub.processConnection
    expr = conn.sqlrepr(sqlbuilder.Select(['id'], staticTables=['sentence'], where=where))
    return list(map(lambda e: e[0], conn.queryAll(expr)))

@public
def get_sentence_for_topic_modeling_by_id(sentence_id):
    params.check_not_null(sentence_id)
    _new_connection_if_needed()
    return select_by(SentenceForTopicModeling, id=sentence_id).getOne()

@public
def get_train_test_split(**kwargs):
    return select_one_by(TrainTestSplit, **kwargs)

@public
def get_train_test_split_per_project(**kwargs):
    """
    """
    return select_by(TrainTestSplitPerProject, **kwargs)

@public
def get_trie(name):
    params.check_not_null(name)
    _new_connection_if_needed()
    return select_one_by(Trie, name=name)