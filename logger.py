# -*- coding:utf-8 -*-
from annotations import public, private

import config
import logging

logging.basicConfig(\
    format=config.get_value(['log', 'format']),
    handlers=[
        logging.FileHandler(config.get_value(['log', 'file'])),
        logging.StreamHandler()
    ])
LOGGER = logging.getLogger()
LOGGER.setLevel(config.get_value(['log', 'level']))

CRITICAL = logging._nameToLevel['CRITICAL']
DEBUG = logging._nameToLevel['DEBUG']
INFO = logging._nameToLevel['INFO']
WARN =  logging._nameToLevel['WARNING']

@private
def _log(msg, level=INFO):
    LOGGER.log(level, msg)

@public
def is_debug_enabled():
    return LOGGER.isEnabledFor(DEBUG)

@public
def info(msg):
    _log(msg, level=INFO)

@public
def debug(msg):
    _log(msg, level=DEBUG)

@public
def warn(msg):
    _log(msg, level=WARN)

@public
def fatal(msg):
    _log(msg, level=CRITICAL)