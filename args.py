# -*- coding:utf-8 -*-
from annotations import public, private
import params
import utils

import argparse

@public
class ArgParser(object):
    """
        Argument parsing helper class.
    """
    def __init__(self, **kwargs):
        """
            Constructor.
        """
        self._parser = argparse.ArgumentParser(**kwargs)

    @public
    def add_xor_group(self, **kwargs):
        """
            Adds a mutually exclusive group to the argument parser.
        """
        return self._parser.add_mutually_exclusive_group(**kwargs)

    @public
    def add_group(self, *args, **kwargs):
        """
            Adds an argument group to the argument parser.
        """
        return self._parser.add_argument_group(*args, **kwargs)

    @private
    def _add_argument_to_group(self, group, *args, **kwargs):
        """
            Adds an argument to a group.
        """
        params.check_not_null(group)
        group.add_argument(*args, **kwargs)

    @public
    def add_argument(self, name, group=None, **kwargs):
        """
            Adds a named argument to a group.
            
            Parameters
            ----------
            group: argument group
            arg: argument name
            action:  what to do when the parser encounters the given option
            target: function from which to get the docs on order to build the help string
        """
        if (group is None):
            self._parser.add_argument(name, **kwargs)
        else:
            group.add_argument(group, name, **kwargs)

    @public
    def add_bool_argument(self, group, name, target=None, default=False):
        """
            Adds an argument.
        """
        if (group is None):
            self._parser.add_argument(name, action='store_true', help=utils.get_doc(target), default=default)
        else:
            group.add_argument(name, action='store_true', help=utils.get_doc(target))
    
    @public
    def parse(self):
        """
            Parses arguments.

            Return
            ----------
            the parsed arguments namespace
        """
        return self._parser.parse_args()