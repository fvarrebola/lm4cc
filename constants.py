# -*- coding:utf-8 -*-
BIN = 'bin'
BOS = '<BOS>'
CSV = 'csv'
ENCODING = 'utf-8'
EMPTY = ''
EOS = '<EOS>'
GIT = '.git'
INF = float('-inf')
JAVA = '.java'
JSON = '.json'
MAX_CHARS = 80
MAX_HITS = 5
DEFAULT_NGRAM_ORDER = 3
MIN_NGRAM_ORDER = 1
MAX_NGRAM_ORDER = 10
MAX_CANDIDATES = 10
NULL_CHAR = '\0'
RANDOM_STATE = 3
LRU_CACHE_MAX_SIZE = 8192

SPACE = ' '
STARS = '*' * MAX_CHARS
SLICES_QTY= 4
UNDEF = -1

TYPE_GLOBAL = 'global'
TYPE_PER_PROJECT = 'per-project'
TYPE_PER_PROJECT_TOPIC = 'per-project-topic'
TYPE_PER_FILE = 'per-file'
TYPE_PER_FILE_TOPIC = 'per-file-topic'

FILTER_NONE = 'None'
FILTER_IDENTIFIER = 'Identifier'
FILTER_IDENTIFIER_LITERAL = 'Identifier+Literal'
FILTER_IDENTIFIER_STRING = 'Identifier+String'
FILTER_LITERAL = 'Literal'
FILTER_STRING = 'String'
FILTER_SEPARATOR = 'Separator'
FILTER_NAMES = [FILTER_NONE, FILTER_IDENTIFIER, FILTER_STRING, FILTER_LITERAL, FILTER_IDENTIFIER_STRING, FILTER_IDENTIFIER_LITERAL]

JSON_REPOSITORY_LIST = 'repositories.json'
JSON_CLONES = 'clones.json'
JSON_CLONING_FAILURES = 'failures.json'

REPOSITORY_LIST = 'repositories'
PROJECT_LIST = 'projects'
FILE_LIST = 'files'
NORMALIZED_FILE_LIST = 'normalized-files'
PARSEABLE_FILE_LIST = 'parseable-files'
EMPTY_FILE_LIST = 'empty-files'
INVALID_FILE_LIST = 'invalid-files'
NON_PARSEABLE_FILE_LIST = 'non-parseable-files'
STATISTICS = 'parseable-files-statistics'
SENTENCES = 'sentences'
TRAINING_SENTENCES = 'training-sentences'
TEST_SENTENCES = 'test-sentences'
VOCABULARY = 'vocabulary'

LOWER_THRESHOLD = -1
UPPER_THRESHOLD = -1

EXIT_CODE_SUCCESS = 0
EXIT_CODE_CANCELLED = -1
EXIT_CODE_UNKNOWN_OPT = -2

