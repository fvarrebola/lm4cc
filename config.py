# -*- coding:utf-8 -*-
from annotations import public, private
import constants
import params

import json
import os
import tempfile

_CONFIG = dict()

@public
def load(filename):
    params.check_not_null(filename)
    if constants.JSON not in filename:
        filename += constants.JSON
    with open(os.path.join(os.path.curdir, 'config', filename), 'r') as fp:
        return json.load(fp)

@private
def _get_working_dir(dir):
    root = get_value(['working-dirs', 'root'], default=tempfile.gettempdir())
    path = os.path.join(root, get_value(['working-dirs', dir]))
    if not os.path.exists(path):
        os.mkdir(path)
    return path


@public
def get_value(keys, default=None):
    """
        Gets a config value.
    """
    value = None
    config = _CONFIG
    for k in keys:
        try:
            value = config = config[k]
        except KeyError:
            pass
    if value is None:
        if default is None:
            raise ValueError("Keys '%s' were not found and a default value was not set" % keys)
        else:
            value = default
    return value


@public
def get_clones_dir():
    return _get_working_dir('clones')

@public
def get_filtered_dir():
    return _get_working_dir('filtered')

@public
def get_normalized_dir():
    return _get_working_dir('normalized')

@public
def get_csv_dir():
    return _get_working_dir('csv')

@public
def get_bin_dir():
    return _get_working_dir('pickle')

@public
def get_models_dir():
    return _get_working_dir('models')

@public
def get_sql_url():
    return get_value(['sql', 'url'])


@public
def get_sql_debug():
    return get_value(['sql', 'debug'])

@public
def get_docker_container_id():
    return get_value(['docker', 'container'])

_CONFIG = load('global')